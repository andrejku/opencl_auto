/**
	\file automaton_CL.h
	\brief declaration of automaton_CL struct
*/

#pragma once
#include <CL/cl.hpp>		// OpenCl C++ API
#include "automaton_host.h"	// automaton_host class

namespace cl_auto
{

/**
	\brief represenatation of automaton on OpenCL device
	
	Holds memory object for automaton table on device
*/
typedef struct automaton_CL
{
	int state_bits;		///< number of bits to represent state
	int input_bits;		///< number of bits to represent input symbol
	int output_bits;	///< number of bits to represent output symbol

	/**
		\brief transition table (stored on the device)
		Format - same as in automaton_host
	*/
	cl::Buffer table;	

	/** 
		\brief pointer to the context, to which automaton is associated
	*/
	const cl::Context* context;
	
	/**
		\brief creates representation of automaton on OpenCL device
		\param host_automaton representation of automaton in host memory
		\param target_context context, to which automaton table buffer will be associated

		\warning Errors will be checked by assert-like statement
		\warning caller is responsible that the context will not be destroyed
		before this automaton_CL structure
	*/
	automaton_CL(
		const common_auto::automaton_host& host_automaton, 
		const cl::Context& target_context
	);

	/**
		\brief empty destructor
		cl::Buffer object should be automatically destroyed and memory on device will be freed
	*/
	~automaton_CL();
private:
	/**
		\brief prohibit copy constructor
	*/
	automaton_CL(const automaton_CL& other);
	automaton_CL& operator=(const automaton_CL& other);
} automaton_CL;

}