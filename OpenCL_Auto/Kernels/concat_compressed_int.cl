#define SHIFT_BITS 4
#define SHIFT_MASK 0xFU
#define PACKED_SYMBOLS_PER_UINT32 8

void kernel run_automaton_compressed_int(
	const global unsigned int* table,
	int word_length,
	int word_count,
	int state_bits,
	const global unsigned int* input,
	global unsigned int* output
)
{
	int global_id=get_global_id(0);
	unsigned int state_mask=(1U<<state_bits)-1U;
	unsigned int state=0U;

	unsigned int input_fetched;
	unsigned int output_combined;

	unsigned char input_small;
	unsigned char output_small;
	
	if(global_id < word_count)
	{
		input+=global_id*word_length;
		output+=global_id*word_length;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=input[i];
			output_combined=0U;
			
			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*j)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*j);
			}

			output[i]=output_combined;
		}
	}
}