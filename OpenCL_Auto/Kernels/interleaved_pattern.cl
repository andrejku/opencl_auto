#define SHIFT_BITS 4
#define SHIFT_MASK 0xF
#define PACKED_SYMBOLS_PER_UINT32 8

#define WORKGROUP_SIZE 64

// asuming that words are concatenated
#define i_th_word_j_th_unit(words, word_length, i, j) (words)[ (i) * (word_length) + (j) ] 

// a not-so correct but working minimum macro
#define my_min(a,b) (a) < (b) ? (a) : (b)

void kernel run_automaton_interleaved_pattern(
	const global unsigned int* table,
	int word_length,
	int word_count,
	int state_bits,
	const global unsigned int* input,
	global unsigned int* output
)
{
	int local_id=get_local_id(0);
	int group_offset=get_local_size(0)*get_group_id(0);
	int steps_to_make=my_min(WORKGROUP_SIZE, word_count-group_offset); 

	unsigned int state_mask=(1U<<state_bits)-1U;
	unsigned int state=0U;

	unsigned int input_fetched;
	unsigned int output_combined;

	unsigned char input_small;
	unsigned char output_small;

	__local unsigned int input_and_output[WORKGROUP_SIZE][WORKGROUP_SIZE];
	
	for(int current=0; current<word_length; current+=WORKGROUP_SIZE)
	{
		int thread_symbol=current+local_id;
		bool thread_loads_memory=thread_symbol < word_length;

		// collaborative load phase		
		if(thread_loads_memory)
		{
			for(int j=0; j<steps_to_make; j++)
			{
				int current_word = group_offset+j;
				input_and_output[j][local_id]=
					i_th_word_j_th_unit(input, word_length, current_word, thread_symbol);
			}
		}

		barrier(CLK_LOCAL_MEM_FENCE);
		for(int j=0; j<WORKGROUP_SIZE; j++)
		{
			input_fetched=input_and_output[local_id][j];
			output_combined=0U;
			
			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int jj=0; jj<PACKED_SYMBOLS_PER_UINT32; jj++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*jj)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*jj);
			}

			input_and_output[local_id][j]=output_combined;
		}

		barrier(CLK_LOCAL_MEM_FENCE);
		if(thread_loads_memory)
		{
			for(int j=0; j<steps_to_make; j++)
			{
				int current_word = group_offset+j;
				i_th_word_j_th_unit(output, word_length, current_word, thread_symbol)=input_and_output[j][local_id];
			}
		}
	}
}