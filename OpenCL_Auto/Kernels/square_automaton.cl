#define SQUARE_SHIFT_BITS 8
#define SQUARE_SHIFT_MASK 0xFFU
#define SQUARE_PACKED_SYMBOLS_PER_UINT32 4

void kernel run_square_automaton_uint32(
	const global unsigned int* table,
	int word_length, // number of uint32_t units in each word
	int word_count,
	int state_bits,
	const global unsigned int* input,
	global unsigned int* output
)
{
	int global_id=get_global_id(0);
	unsigned int state_mask=(1U<<state_bits)-1U; // mask to get lower state_bits bits
	unsigned int state=0U; // all threads start from state 0

	unsigned int input_fetched; // 8 input symbols fetched once at a time
	unsigned char input_small; // 1 current input symbol
	unsigned char output_small; // 1 current output symbol
	unsigned int output_combined; // will hold 8 output symbols

	// important if word count is not divisible by work group size
	if(global_id < word_count)
	{
		input+=global_id;
		output+=global_id;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input; // load input from global memory
			output_combined=0U;

			#pragma unroll SQUARE_PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<SQUARE_PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SQUARE_SHIFT_BITS*j)) & SQUARE_SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SQUARE_SHIFT_BITS*j);
			}

			*output=output_combined;

			// move input and output pointer 
			// by word_count unsigned int units forward
			input+=word_count; 
			output+=word_count;
		}
	}
}