void kernel only_automaton_access_dummy(
	global const unsigned int* table,
	int word_length,
	int word_count,
	int state_bits,
	int input_bits,
	global const unsigned char* input,
	global unsigned char* output
)
{
	int global_id = get_global_id(0);
	
	unsigned int state_mask = (1U<<state_bits)-1U;
	unsigned int state=0U;

	unsigned char dummy_input;
	unsigned char dummy_result=0;

	unsigned int rand_0=global_id+5U;
	unsigned int rand_1=global_id*global_id+1U;
	unsigned int rand_2;

	int full_len=word_length>>2;

	unsigned int input_mask=1U<<input_bits-1U;

	if(global_id < word_count)
	{
		for(int i=0; i<full_len; i++)
		{
			rand_2=rand_1*rand_1+rand_0+1U;
			rand_0=rand_1;
			rand_1=rand_2;

			dummy_input=rand_2&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>4)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>8)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>16)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>20)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>24)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>28)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;
		}

		dummy_result=state;
		output[global_id]=dummy_result; // Do not want compiler to optimize memory accesses away
	}
}