#define SHIFT_BITS 4
#define SHIFT_MASK 0xFU
#define PACKED_SYMBOLS_PER_UINT32 8

const sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE;

void kernel run_automaton_texture_words_interleaved_uint32(
	read_only image2d_t table,
	int word_length,
	int word_count,
	int state_bits,
	const global unsigned int* input,
	global unsigned int* output
)
{
	int global_id=get_global_id(0);
	unsigned int state_mask=(1U<<state_bits)-1U;
	unsigned int state=0U;

	unsigned int input_fetched;
	unsigned char input_small;
	unsigned char output_small;
	unsigned int output_combined;

	unsigned int table_pos;

	uint4 tex;
	int shift_val;

	if(global_id < word_count)
	{
		input+=global_id;
		output+=global_id;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input;
			output_combined=0U;

			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*j)) & SHIFT_MASK;
				table_pos = (input_small << state_bits) | state;
				tex = read_imageui(table, sampler, (int2)(table_pos, 0));
				state=tex.x | (tex.y << 8) | (tex.z << 16) | (tex.w << 24);
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*j);
			}
			
			*output=output_combined;

			input+=word_count;
			output+=word_count;
		}
	}
}