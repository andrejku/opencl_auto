#pragma once

#include "automaton_CL.h"	// automaton_CL declaration
#include "CL_helper.h"		// cl_assert
#include <stddef.h>			// NULL
#include <cassert>			// assert

using common_auto::automaton_host;

namespace cl_auto
{
automaton_CL::automaton_CL(
	const automaton_host& host_automaton, 
	const cl::Context& target_context
)
{
	state_bits=host_automaton.state_bits;
	input_bits=host_automaton.input_bits;
	output_bits=host_automaton.output_bits;
	context=&target_context;

	cl_int table_alloc_error;

	table = cl::Buffer(
		target_context, 
		CL_MEM_READ_ONLY, 
		sizeof(*host_automaton.table)*host_automaton.table_size,
		NULL,
		&table_alloc_error
	);

	cl_assert(table_alloc_error);

	cl::CommandQueue queue(target_context);
	cl_int table_copy_error = queue.enqueueWriteBuffer(
		table,
		//CL_TRUE, 
		CL_FALSE,
		0,
		sizeof(*host_automaton.table)*host_automaton.table_size,
		(void*) host_automaton.table
	);

	cl_assert(table_copy_error);
	
	cl_int queue_finish_error = queue.finish();
	cl_assert(queue_finish_error);
	//return result;
}


automaton_CL::~automaton_CL()
{
	
}

}