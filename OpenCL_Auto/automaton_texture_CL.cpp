
#include "automaton_texture_CL.h"
#include "CL_helper.h"
#include <cassert>


using common_auto::automaton_host;

namespace cl_auto
{
automaton_texture_CL::automaton_texture_CL(
	const common_auto::automaton_host& host_automaton, 
	const cl::Context& target_context
)
{
	state_bits=host_automaton.state_bits;
	input_bits=host_automaton.input_bits;
	output_bits=host_automaton.output_bits;
	context=&target_context;

	cl_int table_alloc_error;
	cl::ImageFormat format(CL_RGBA, CL_UNSIGNED_INT8);
	
	table=cl::Image2D(
		target_context,
		CL_MEM_READ_ONLY,
		format,
		host_automaton.table_size,
		1,
		0,
		NULL,
		&table_alloc_error
	);

	
	cl::size_t<3> origin;
	origin[0]=0;
	origin[1]=0;
	origin[2]=0;

	cl::size_t<3> region;
	region[0]=host_automaton.table_size;
	region[1]=1;
	region[2]=1;

	cl_assert(table_alloc_error);
	cl::CommandQueue queue(target_context);
	
	cl_int table_copy_error=queue.enqueueWriteImage(
		table,
		CL_FALSE,
		origin,
		region,
		0,
		0,
		(void*) host_automaton.table
	);
	cl_assert(table_copy_error);
	
	cl_int queue_finish_error = queue.finish();
	cl_assert(queue_finish_error);
}

automaton_text_CL::~automaton_texture_CL()
{

}

}