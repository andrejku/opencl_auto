/**
	\file cl_run_automaton.h
	\brief declares functions that simulate automaton on word complect
*/

// TODO - better comment

#pragma once
#include <cstdint>			// intx_t
#include "constants.h"
#include "automaton_CL.h"	// automaton_CL struct
#include "automaton_texture_CL.h"
#include "cl_pinned_words.h"

// TODO - why functions return cl_int?

/**
	In all functions:
	- word_length doesn't mean number of symbols in word, it means number of symbol units
	(so input is sizeof(*input)*word_length*word_count large memory block
	- kernel is expected to be created from "expected" text (see uses in Test project)
	- automaton and kernel belong to same cl::Context
	- THREADS_PER_WORKGROUP - if you do not want to test which workgroup size is better, just use 64

	P.S. Most of the functions are heavily copypasted (heavy defines would make reading hard,
	additional function calls are not used considering performance as main goal)
*/

namespace cl_auto
{

cl_int cl_run_automaton_words_concatenated_compressed_int(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
);


cl_int cl_run_automaton_words_concatenated_interleaved_access(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output
);

/**
	\brief run automaton on 32-bit interleaved words
	\param automaton representation in OpenCL device
	\param kernel and automaton should be asociated to same context
			and be compiled from expected text (see uses in testing application)
	\param word_count number of different words in a complect
	\param word_length number of 32-bit, 8-symbol units in each word
	\param input pointer to input words in host memory
	\param output pointer to output words in host memory
	\param THREADS_PER_WORKGROUP will yield different performance on different devices

	Input and output symbols will be 32-bit interleaved
	Symbols in each uint32_t unit will be stored in little-endian mode (4 bits per symbol)
	\warning possible errors are cheched using assert-like statement
*/
cl_int cl_run_automaton_symbols_interleaved_uint32(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
);

cl_int cl_run_square_automaton(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
);

// Warning - pinned memory usage with OpenCL was
// implemented by trial and error method, so use at your own risk! 
cl_int cl_run_pinned_words(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	cl_pinned_words& word_complect,
	const int THREADS_PER_WORKGROUP
);

// I am not sure, if texture memory is used in absolutely right way (from performance point of view)
// (again, trial and error method...)
cl_int cl_run_texture_automaton(
	const automaton_text_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
);

cl_int cl_only_automaton_access_dummy(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint8_t* input,
	uint8_t* output,
	const int THREADS_PER_WORKGROUP
);


}