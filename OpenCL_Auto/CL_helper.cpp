#pragma once

#include <CL/cl.hpp>		// OpenCL
#include <string>			// std::string
#include <fstream>			// std::ifstream
#include <iostream>			// std::cout
#include <sstream>			// std::stringstream
#include <vector>			// std::vector
#include "CL_helper.h"		// function definitions
#include <cassert>

namespace cl_auto
{

cl::Context get_all_GPU_context(void)
{
	return cl::Context(CL_DEVICE_TYPE_GPU);
}

cl::Context get_first_GPU_context(void)
{
	std::vector<cl::Platform> all_platforms;
	cl_int get_platform_error=cl::Platform::get(&all_platforms);
	cl_assert(get_platform_error);

	cl::Device gpu_device;
	bool found_device=false;

	for(auto& platform: all_platforms)
	{
		std::vector<cl::Device> available_gpu_devices;
		cl_int get_devices_error = platform.getDevices(
			CL_DEVICE_TYPE_GPU, 
			&available_gpu_devices
		);
		
		if(!available_gpu_devices.empty() && get_devices_error==CL_SUCCESS)
		{
			gpu_device = available_gpu_devices[0];
			found_device=true;
			break;
		}
	}

	assert(found_device);
	cl::Context result(gpu_device);
	return result;
}

cl::Context get_first_CPU_context(void)
{
	std::vector<cl::Platform> all_platforms;
	cl_int get_platform_error=cl::Platform::get(&all_platforms);
	cl_assert(get_platform_error);

	cl::Device cpu_device;
	bool found_device=false;

	for(auto& platform: all_platforms)
	{
		std::vector<cl::Device> available_gpu_devices;
		cl_int get_devices_error = platform.getDevices(
			CL_DEVICE_TYPE_CPU, 
			&available_gpu_devices
		);
		
		if(!available_gpu_devices.empty() && get_devices_error==CL_SUCCESS)
		{
			cpu_device = available_gpu_devices[0];
			found_device=true;
			break;
		}
	}

	assert(found_device);
	cl::Context result(cpu_device);
	return result;
}

cl::Program compile_program(
	const std::vector<std::string>& source_filenames, 
	const cl::Context& target_context,
	const std::string& build_options
)
{
	const static std::string FILE_NOT_FOUND = "Kernel source file not found";
	const static std::string kernel_source_path="../OpenCL_Auto/Kernels/";
	
	std::vector<std::string> kernel_texts;

	for(auto& source_name: source_filenames)
	{
		std::ifstream source_file_stream(kernel_source_path+source_name);
		if(source_file_stream)
		{
			std::stringstream source_extracted;
			source_extracted << source_file_stream.rdbuf();
			std::string kernel_text = source_extracted.str();
			kernel_texts.push_back(kernel_text);
		}
		else
		{
			std::cerr << FILE_NOT_FOUND << ": " << source_name << std::endl;
			abort();
		}
		source_file_stream.close();
	}

	cl::Program::Sources all_sources;
	for(auto& kernel_text: kernel_texts)
	{
		all_sources.push_back(
			std::make_pair(kernel_text.c_str(), kernel_text.length())
		);
	}

	cl_int create_error;
	cl::Program result = cl::Program(target_context, all_sources, &create_error);
	cl_assert(create_error); // error creating program from sources

	cl_int context_devices_error;
	std::vector<cl::Device> target_devices = 
		target_context.getInfo<CL_CONTEXT_DEVICES>(&context_devices_error);
	cl_assert(context_devices_error); // error getting devices from context

	cl_int build_error=result.build(target_devices, build_options.c_str());
	if(build_error!=CL_SUCCESS)
	{
		std::string build_log = 
			result.getBuildInfo<CL_PROGRAM_BUILD_LOG>(target_devices[0]);

		std::cerr << build_log << std::endl; // output build log error
	}
	cl_assert(build_error); // error building program for devices

	return result;
}

cl::Kernel create_kernel(
	const cl::Context& target_context,
	const std::string& source_filename,
	const std::string& kernel_name,
	const std::string& build_options
)
{
	std::vector<std::string> source_filenames;
	source_filenames.push_back(source_filename);

	cl::Program program = compile_program(
		source_filenames,
		target_context,
		build_options
	);

	cl_int make_kernel_error;
	cl::Kernel result=cl::Kernel(program, kernel_name.c_str(), &make_kernel_error);
	cl_assert(make_kernel_error); // error creating kernel from program

	return result;
}

/*
cl::Kernel create_kernel(
	const std::vector<std::string>& source_filenames,
	const cl::Context& target_context,
	const std::string& kernel_name,
	const std::string& build_options
)
{
	cl::Program program = compile_program(
		source_filenames,
		target_context,
		build_options
	);

	cl_int make_kernel_error;
	cl::Kernel result=cl::Kernel(program, kernel_name.c_str(), &make_kernel_error);
	cl_assert(make_kernel_error); // error creating kernel from program

	return result;
}
*/

inline void cl_check_error(
	cl_int error,
	const char* file, 
	int line, 
	bool abort,
	const std::string& message)
{
	if(error != CL_SUCCESS)
	{
		std::cerr << "OpenCL error: " << clewErrorString(error) << std::endl;
		if(message!="") std::cerr << "Message is: " << message << std::endl;
		std::cerr << "Location of error: " 
			<< file << " at line: " << line << std::endl;
		if(abort) exit(error);
	}
}

const char* clewErrorString(cl_int error)
{
    static const char* strings[] =
    {
        // Error Codes
          "CL_SUCCESS"                                  //   0
        , "CL_DEVICE_NOT_FOUND"                         //  -1
        , "CL_DEVICE_NOT_AVAILABLE"                     //  -2
        , "CL_COMPILER_NOT_AVAILABLE"                   //  -3
        , "CL_MEM_OBJECT_ALLOCATION_FAILURE"            //  -4
        , "CL_OUT_OF_RESOURCES"                         //  -5
        , "CL_OUT_OF_HOST_MEMORY"                       //  -6
        , "CL_PROFILING_INFO_NOT_AVAILABLE"             //  -7
        , "CL_MEM_COPY_OVERLAP"                         //  -8
        , "CL_IMAGE_FORMAT_MISMATCH"                    //  -9
        , "CL_IMAGE_FORMAT_NOT_SUPPORTED"               //  -10
        , "CL_BUILD_PROGRAM_FAILURE"                    //  -11
        , "CL_MAP_FAILURE"                              //  -12

        , ""											//  -13
        , ""											//  -14
        , ""											//  -15
        , ""											//  -16
        , ""											//  -17
        , ""											//  -18
        , ""											//  -19

        , ""											//  -20
        , ""											//  -21
        , ""											//  -22
        , ""											//  -23
        , ""											//  -24
        , ""											//  -25
        , ""											//  -26
        , ""											//  -27
        , ""											//  -28
        , ""											//  -29

        , "CL_INVALID_VALUE"                            //  -30
        , "CL_INVALID_DEVICE_TYPE"                      //  -31
        , "CL_INVALID_PLATFORM"                         //  -32
        , "CL_INVALID_DEVICE"                           //  -33
        , "CL_INVALID_CONTEXT"                          //  -34
        , "CL_INVALID_QUEUE_PROPERTIES"                 //  -35
        , "CL_INVALID_COMMAND_QUEUE"                    //  -36
        , "CL_INVALID_HOST_PTR"                         //  -37
        , "CL_INVALID_MEM_OBJECT"                       //  -38
        , "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR"          //  -39
        , "CL_INVALID_IMAGE_SIZE"                       //  -40
        , "CL_INVALID_SAMPLER"                          //  -41
        , "CL_INVALID_BINARY"                           //  -42
        , "CL_INVALID_BUILD_OPTIONS"                    //  -43
        , "CL_INVALID_PROGRAM"                          //  -44
        , "CL_INVALID_PROGRAM_EXECUTABLE"               //  -45
        , "CL_INVALID_KERNEL_NAME"                      //  -46
        , "CL_INVALID_KERNEL_DEFINITION"                //  -47
        , "CL_INVALID_KERNEL"                           //  -48
        , "CL_INVALID_ARG_INDEX"                        //  -49
        , "CL_INVALID_ARG_VALUE"                        //  -50
        , "CL_INVALID_ARG_SIZE"                         //  -51
        , "CL_INVALID_KERNEL_ARGS"                      //  -52
        , "CL_INVALID_WORK_DIMENSION"                   //  -53
        , "CL_INVALID_WORK_GROUP_SIZE"                  //  -54
        , "CL_INVALID_WORK_ITEM_SIZE"                   //  -55
        , "CL_INVALID_GLOBAL_OFFSET"                    //  -56
        , "CL_INVALID_EVENT_WAIT_LIST"                  //  -57
        , "CL_INVALID_EVENT"                            //  -58
        , "CL_INVALID_OPERATION"                        //  -59
        , "CL_INVALID_GL_OBJECT"                        //  -60
        , "CL_INVALID_BUFFER_SIZE"                      //  -61
        , "CL_INVALID_MIP_LEVEL"                        //  -62
        , "CL_INVALID_GLOBAL_WORK_SIZE"                 //  -63
        , "CL_UNKNOWN_ERROR_CODE"
    };

    if (error >= -63 && error <= 0)
         return strings[-error];
    else
         return strings[64];
}

}