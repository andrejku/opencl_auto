/**
	\file CL_helper.h
	\brief Helper functions for OpenCL automaton implementation
*/

#pragma once
#include <CL/cl.hpp>	// OpenCL
#include <vector>		// std::vector
#include <string>		// std::string
#include <array>		// std::string

namespace cl_auto
{

/**
	\brief returns context with all GPU devices
*/
cl::Context get_all_GPU_contexts(void);

/** 
	\brief gets context for (some) first available GPU device
	\warning Errors will be checked by assert-like statement
*/
cl::Context get_first_GPU_context(void);

/** 
	\brief gets context for (some) first available CPU device
	\warning Errors will be checked by assert-like statement
*/
cl::Context get_first_CPU_context(void);

/**
	\brief Gets meaninigful string for OpenCL error code
	\param error code for error

	The code behind is from CLEW library:
	https://code.google.com/p/clew/source/browse/src/clew.c
*/
const char* clewErrorString(cl_int error);

// TODO - comment on options
/**
	\brief Creates cl::Program from all given source files
	\param source_filenames list of kernel names
	\param target_context contains devices for which program will be made
	\param build_options 
	\attention TODO
*/
/*
cl::Program compile_program(
	const std::vector<std::string>& source_filenames,
	//const std::array<std::string, SIZE>& source_filenames,
	const cl::Context& target_context,
	const std::string& build_options=""
);
*/

cl::Kernel create_kernel(
	const cl::Context& target_context,
	const std::string& source_filename,
	const std::string& kernel_name,
	const std::string& build_options
);

/*
cl::Kernel create_kernel(
	const std::vector<std::string>& source_filenames,
	const cl::Context& target_context,
	const std::string& kernel_name,
	const std::string& build_options
);
*/

/** 
	\brief assert macro
	\param error of type cl_int

	Errors will be printed to std::cerr
*/
#define cl_assert(error) {									\
	cl_check_error( (error), __FILE__, __LINE__, true );	\
}															\

/** 
	\brief assert macro with message
	\param error of type cl_int
	\param message additional info of type std::string

	Errors will be printed to std::cerr
*/
#define cl_assert_msg(error, message) {									\
	cl_check_error( (error), __FILE__, __LINE__, true, (message) );		\
}																		\

/** 
	\brief function "behind" assert macros
	\param error error to be checked
	\param file location of error
	\param line location of error
	\param abort whether to quit or not
	\param message additional information
*/
inline void cl_check_error(
	cl_int error,
	const char* file, 
	int line, 
	bool abort,
	const std::string& message=""
);

}