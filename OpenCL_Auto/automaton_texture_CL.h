/**
	\file automaton_text_CL.h
	\brief declaration of automaton_text_CL struct
*/

#pragma once
#include <CL/cl.hpp>
#include "automaton_host.h"
#include "CL_helper.h"

namespace cl_auto
{

// TODO - comment
/**
	\brief representation of automaton in OpenCL texture??? memory
*/
typedef struct automaton_texture_CL
{
	int state_bits;		///< number of bits to represent state
	int input_bits;		///< number of bits to represent input symbol
	int output_bits;	///< number of bits to represent output symbol

	// TODO - comment?
	/**
		\brief transition table (stored on the device)
	*/
	cl::Image2D table;

	/** 
		\brief pointer to the context, to which automaton is associated
	*/
	const cl::Context* context;

	/**
		\brief constructs automaton representation on OpenCL device
		\param host_automaton representation of automaton in host memory
		\param target_context context, to which automaton table buffer will be associated
	*/
	automaton_texture_CL(
		const common_auto::automaton_host& host_automaton, 
		const cl::Context& target_context
	);

	/**
		\brief memory allocated on the device should be freed automatically
	*/
	~automaton_texture_CL();

private:
	/**
		\brief prohibit copy constructor
	*/
	automaton_texture_CL(const automaton_texture_CL& other);
	automaton_texture_CL& operator=(const automaton_texture_CL& other);
} automaton_text_CL;

}