#pragma once

#include <CL/cl.h>
#include "cl_run_automaton.h"
#include "CL_helper.h"
#include "random_functions.h"
#include <vector>
#include <map>
#include <iostream> 
#include <cassert>

using common_auto::ceiling;
using common_auto::time_diff;

namespace cl_auto
{

cl_int cl_run_automaton_words_concatenated_compressed_int(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY, 
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, 
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	cl::CommandQueue queue(*automaton.context);

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, output_buffer);
	cl_assert(set_arg_err);

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, 
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

	return 0;
}

const int INTERLEAVED_PATTERN_GROUP_SIZE=64;

cl_int cl_run_automaton_words_concatenated_interleaved_access(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY, 
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, 
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	cl::CommandQueue queue(*automaton.context);

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, output_buffer);
	cl_assert(set_arg_err);

	const int THREADS_PER_WORKGROUP=64;

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, 
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

	return 0;
}

//#define OUTPUT_INFO_LAUNCH_CL

cl_int cl_run_automaton_symbols_interleaved_uint32(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

#ifdef OUTPUT_INFO_LAUNCH_CL
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY, // buffer will only be read from during kernel execution
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, // buffer will only be written to during kernel execution
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	// memory transfers will be scheduled to this queue
	cl::CommandQueue queue(*automaton.context); 

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	// set each argument for kernel
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, output_buffer);
	cl_assert(set_arg_err);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
#endif

	// calculate kernel launch size
	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	
	// global range 1st dimension should be divisible by group size 
	// and be at least as big, as word count
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP 
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	kernel_time=time_diff();
#endif

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, // wait, until output will be copied back to host memory
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
	std::cout << "Kernel time, other time: " <<  kernel_time << ", " << other_time << '\n';
#endif

	return 0;
}

cl_int cl_run_square_automaton(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
)
{
	assert(
		automaton.input_bits <= SQUARE_SHIFT_BITS && 
		automaton.output_bits <= SQUARE_SHIFT_BITS
	);

#ifdef OUTPUT_INFO_LAUNCH_CL
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY,
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, 
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	cl::CommandQueue queue(*automaton.context); 

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, output_buffer);
	cl_assert(set_arg_err);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
#endif

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP 
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	kernel_time=time_diff();
#endif

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, // wait, until output will be copied back to host memory
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
	std::cout << "Kernel time, other time: " <<  kernel_time << ", " << other_time << '\n';
#endif

	return 0;
}

cl_int cl_run_pinned_words(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	cl_pinned_words& word_complect,
	const int THREADS_PER_WORKGROUP
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);
	
#ifdef OUTPUT_INFO_LAUNCH_CL
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif
	assert(word_count*word_length*sizeof(uint32_t) <= word_complect.buffer_length);
	
	cl::CommandQueue queue(*automaton.context);
	/*
	cl_int err = queue.enqueueUnmapMemObject(
		word_complect.input_words_pinned,
		NULL, //word_complect.map_input(),
		NULL,
		NULL
	);

	err=queue.enqueueUnmapMemObject(
		word_complect.output_words_pinned,
		NULL, //word_complect.map_output(),
		NULL,
		NULL
	);
	*/
	word_complect.prepare_for_device_access(queue);
	
	cl_int set_arg_err;
	
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, word_complect.input_words_pinned);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, word_complect.output_words_pinned);
	cl_assert(set_arg_err);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
#endif

	/*
	cl_int copy_output_error = queue.enqueueWriteBuffer(
		
	);
	*/

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	kernel_time=time_diff();
#endif

	/*
	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, 
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);
	*/

#ifdef OUTPUT_INFO_LAUNCH_CL
	queue.finish();
	other_time+=time_diff();
	std::cout << "Kernel time, other time: " << kernel_time << ' ' << other_time << '\n';
#endif

	//queue.finish();
	//word_complect.map_output();
	word_complect.prepare_for_host_access(queue);
	queue.finish();
	return 0;
}

cl_int cl_run_texture_automaton(
	const automaton_texture_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_WORKGROUP
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY, 
		//CL_MEM_READ_WRITE, // --remove, HCK!!!
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, 
		//CL_MEM_READ_WRITE, // --remove, HCK!!!
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	cl::CommandQueue queue(*automaton.context);

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, output_buffer);
	cl_assert(set_arg_err);

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, 
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

	return 0;
}

cl_int cl_only_automaton_access_dummy(
	const automaton_CL& automaton,
	cl::Kernel& kernel,
	int word_count,
	int word_length,
	const uint8_t* input,
	uint8_t* output,
	const int THREADS_PER_WORKGROUP
)
{
	cl_int input_alloc_error;
	cl::Buffer input_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_READ_ONLY, 
		word_count*word_length*sizeof(*input), 
		NULL, &input_alloc_error
	);
	cl_assert(input_alloc_error);
	
	cl_int output_alloc_error;
	cl::Buffer output_buffer = cl::Buffer(
		*automaton.context, 
		CL_MEM_WRITE_ONLY, 
		word_count*word_length*sizeof(*output),
		NULL,
		&output_alloc_error
	);
	cl_assert(output_alloc_error);
	
	cl::CommandQueue queue(*automaton.context);

	cl_int copy_input_error = queue.enqueueWriteBuffer(
		input_buffer, 
		CL_FALSE, 
		0, 
		word_count*word_length*sizeof(*input),
		(const void*) input
	);
	cl_assert(copy_input_error);
	
	cl_int set_arg_err;
	
	set_arg_err = kernel.setArg(0, automaton.table);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(1, word_length);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(2, word_count);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(3, automaton.state_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(4, automaton.input_bits);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(5, input_buffer);
	cl_assert(set_arg_err);

	set_arg_err = kernel.setArg(6, output_buffer);
	cl_assert(set_arg_err);

	cl::NDRange workgroup_range=cl::NDRange(THREADS_PER_WORKGROUP);
	cl::NDRange global_range=cl::NDRange(
		ceiling(word_count, THREADS_PER_WORKGROUP) * THREADS_PER_WORKGROUP
	);

	cl_int kernel_enqueue_error = queue.enqueueNDRangeKernel(
		kernel, 
		cl::NullRange, 
		global_range, 
		workgroup_range
	);
	cl_assert(kernel_enqueue_error);

	cl_int copy_output_error = queue.enqueueReadBuffer(
		output_buffer, 
		CL_TRUE, 
		0,
		word_count*word_length*sizeof(*output),
		(void*) output
	);
	cl_assert(copy_output_error);

	return 0;
}

}