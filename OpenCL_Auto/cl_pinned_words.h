#pragma once
#include <CL/cl.hpp>
#include "CL_helper.h"

namespace cl_auto
{

// TODO - hide implementation

typedef struct cl_pinned_words
{
private:
	enum words_state 
	{
		NOT_INIT,
		MAPPED_HOST,
		UNMAPPED
	};

	words_state current_state;

	/**
		\brief prohibit copy constructor
	*/
	cl_pinned_words(const cl_pinned_words& other);
	cl_pinned_words& operator=(const cl_pinned_words& other);
public:

	const size_t buffer_length; // in bytes

	cl::Buffer input_words_pinned;
	cl::Buffer output_words_pinned;
	const cl::Context* context;
	
	void* mapped_host_input;
	void* mapped_host_output;

	cl_pinned_words(
		size_t _buffer_length, 
		const cl::Context& target_context
	)
		: buffer_length(_buffer_length),
		context(&target_context),
		current_state(NOT_INIT),
		input_words_pinned(NULL),
		output_words_pinned(NULL)
	{
		cl_int input_alloc_error;
		input_words_pinned = cl::Buffer(
			target_context,
			CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, //CL_MEM_USE_HOST_PTR,
			buffer_length,
			NULL,
			&input_alloc_error
		);
		cl_assert(input_alloc_error);
		
		cl_int output_alloc_error;
		output_words_pinned = cl::Buffer(
			target_context,
			CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, //CL_MEM_USE_HOST_PTR,
			buffer_length,
			NULL,
			&output_alloc_error
		);
		cl_assert(output_alloc_error);
	}

	~cl_pinned_words()
	{
		cl::CommandQueue q(*context); // not sure, if this is a hack
		prepare_for_device_access(q);
		q.finish(); // the same
	}

	void prepare_for_host_access(cl::CommandQueue& schedule_to)
	{
		if(current_state != MAPPED_HOST)
		{
			cl_int map_error;	
			mapped_host_input = 
			schedule_to.enqueueMapBuffer(
				input_words_pinned,
				CL_FALSE,
				CL_MAP_WRITE,
				0,
				buffer_length,
				NULL,
				NULL,
				&map_error
			);
			cl_assert(map_error)

			mapped_host_output = 
			schedule_to.enqueueMapBuffer(
				output_words_pinned,
				CL_TRUE,
				CL_MAP_READ,
				0,
				buffer_length,
				NULL,
				NULL,
				&map_error
			);
			cl_assert(map_error);

			current_state=MAPPED_HOST;
		}
	}

	void prepare_for_device_access(cl::CommandQueue& schedule_to)
	{
		if(current_state == MAPPED_HOST)
		{
			cl_int unmap_error = 
			schedule_to.enqueueUnmapMemObject(
				input_words_pinned,
				mapped_host_input,
				NULL,
				NULL
			);
			cl_assert(unmap_error);

			unmap_error = 
			schedule_to.enqueueUnmapMemObject(
				output_words_pinned,
				mapped_host_output,
				NULL,
				NULL
			);
			cl_assert(unmap_error);

			current_state=UNMAPPED;
		}
	}

	void* map_input(void)
	{
		cl::CommandQueue q(*context);
		prepare_for_host_access(q);
		q.finish();
		return mapped_host_input;
	}

	void* map_output(void)
	{
		cl::CommandQueue q(*context);
		prepare_for_host_access(q);
		q.finish();
		return mapped_host_output;
	}


} cl_pinned_words;

}