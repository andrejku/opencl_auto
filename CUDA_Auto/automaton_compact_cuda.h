/**
	\file automaton_compact_cuda.h
	\brief declaration of automaton_compact_CUDA struct
*/

#pragma once
#include "automaton_host.h"
#include <cstdint>

namespace cuda_auto
{

/**
	\brief compact representation of automaton on CUDA device

	This is a structure that contains a pointer
	to automaton table on CUDA-enabled device
*/
typedef struct automaton_compact_CUDA
{
	int state_bits;
	int input_bits;
	int output_bits;

	/**
		\brief transition table (stored on the device)
		Format - table_compact[i] will be equal to lower_16_bits(table[i])
	*/
	uint16_t* table;

	/**
		\brief creates compact representation of automaton on CUDA device
		\param host_automaton representation of automaton in host memory
		\warning Errors will be checked by assert-like statement
	*/
	automaton_compact_CUDA(
		const common_auto::automaton_host& host_automaton
	);

	/**
		\frees up allocated memory
		\warning Errors will be checked by assert-like statement
	*/
	~automaton_compact_CUDA();

	/**
		\brief checks if table entry could be stored in 2 bytes
		\param host_automaton representation of automaton in host memory
	*/
	static bool fits_in_uint16_t(
		const common_auto::automaton_host& host_automaton
	);

private:
	/**
		\brief prohibit copy constructor
	*/
	automaton_compact_CUDA(const automaton_compact_CUDA& other);
	automaton_compact_CUDA& operator=(const automaton_compact_CUDA& other);
} automaton_compact_CUDA;

}