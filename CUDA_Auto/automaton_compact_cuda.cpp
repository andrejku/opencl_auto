#include "automaton_compact_cuda.h"
#include <cuda_runtime.h>
#include "cuda_helper.h"
#include <cassert>

using common_auto::automaton_host;

namespace cuda_auto
{

automaton_compact_CUDA::automaton_compact_CUDA(
	const common_auto::automaton_host& host_automaton
)
{
	assert(automaton_compact_CUDA::fits_in_uint16_t(host_automaton));

	input_bits=host_automaton.input_bits;
	output_bits=host_automaton.output_bits;
	state_bits=host_automaton.state_bits;

	uint16_t* temp_table=new (std::nothrow) uint16_t[host_automaton.table_size];
	assert(temp_table!=NULL);
	for(int i=0; i<host_automaton.table_size; i++)
	{
		temp_table[i]=host_automaton.table[i];
	}

	cudaError table_alloc_error=cudaMalloc(
		(void**) &(table), 
		host_automaton.table_size*sizeof(*table)
	);
	cuda_assert(table_alloc_error);

	cudaError table_copy_error=cudaMemcpy(
		(void*) table,
		(const void*) temp_table,
		host_automaton.table_size*sizeof(*table),
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);
	cuda_assert(table_copy_error);

	delete[] temp_table;
}

automaton_compact_CUDA::~automaton_compact_CUDA()
{
	cudaError table_free_error=cudaFree(table);
	cuda_assert(table_free_error);
}

bool automaton_compact_CUDA::fits_in_uint16_t(
	const common_auto::automaton_host& host_automaton
)
{
	return
		host_automaton.output_bits+host_automaton.state_bits 
			<= CHAR_BIT*sizeof(uint16_t);
}

}