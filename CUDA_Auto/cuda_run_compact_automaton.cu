#pragma once
#include <cassert>
#include "cuda_run_automaton.h"
#include "random_functions.h"
#include "cuda_helper.h"

using common_auto::ceiling;
using common_auto::time_diff;

namespace cuda_auto
{

__global__ void run_compact_automaton_words_interleaved_uint32_kernel(
	const uint16_t* table,
	int word_count,
	int word_length,
	int state_bits,
	const uint32_t* input,
	uint32_t* output
)
{
	int global_id=blockDim.x*blockIdx.x + threadIdx.x;
	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint32_t input_fetched;
	uint32_t input_small;
	uint32_t output_small;
	uint32_t output_combined;

	if(global_id < word_count)
	{
		//input+=global_id*word_length;
		//output+=global_id*word_length;
		input+=global_id;
		output+=global_id;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input;
			//input_fetched=input[i*word_count+global_id];
			output_combined=0;

			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*j)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*j);
			}

			*output=output_combined;
			input+=word_count;
			output+=word_count;
		}
	}
}

cudaError run_compact_automaton_words_interleaved_uint32(
	const automaton_compact_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint32_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_count*word_length*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);

	cuda_assert(input_copy_error);
	
	dim3 block=dim3(THREADS_PER_BLOCK, 1, 1);
	dim3 grid=dim3(ceiling(word_count, THREADS_PER_BLOCK), 1, 1);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	run_compact_automaton_words_interleaved_uint32_kernel<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		dev_input,
		dev_output
	);

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_count*word_length*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);

	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << kernel_time << ' ' << other_time << '\n';
#endif

	return cudaError::cudaSuccess; // ???
}

}