/**
	\file cuda_helper.h
	\brief Helper functions for CUDA automaton implementation

	Defines assert messages (used for error checking in testing/development)
*/

#pragma once
#include <cuda_runtime.h>		// cudaError
#include <string>				// std::string
#include <iostream>				// std::cerr

namespace cuda_auto
{

/**
	\brief assert macro
	\param error should be of type cudaError

	Errors will be printed to std::cerr
*/
#define cuda_assert(error) {											\
	cuda_check_error( (error), __FILE__, __LINE__, true );				\
}																		\

/**
	\brief assert macro with message
	\param error of type cudaError
	\param message of type std::string

	Errors will be printed to std::cerr
*/
#define cuda_assert_msg(error, message) {								\
	cuda_check_error( (error), __FILE__, __LINE__, true, (message) );	\
}																		\

/** 
	\brief function "behind" assert macros
	\param error error to be checked
	\param file location of error
	\param line location of error
	\param abort whether to quit or not
	\param message additional information
*/
inline void cuda_check_error(
	cudaError error, 
	const char* file,
	int line,
	bool abort,
	const std::string& message="")
{
	if(error != cudaError::cudaSuccess)
	{
		std::cerr << "Cuda error: " << cudaGetErrorString(error) << std::endl;
		if(message!="") std::cerr << "Message is: " << message << std::endl;
		std::cerr << "Location of error: " 
			<< file << " at line: " << line << std::endl;
		if(abort) exit(error);
	}
}

}