/** 
	\file automaton_cuda.h
	\brief declaration of automaton_CUDA struct
*/

#pragma once
#include "automaton_host.h"		// automaton_host class

namespace cuda_auto
{

/**
	\brief representation of automaton on CUDA device

	This is a structure that contains a pointer 
	to automaton table on CUDA-enabled device.
*/
typedef struct automaton_CUDA
{
	int state_bits; 		///< number of bits to represent state
	int input_bits; 		///< number of bits to represent input
	int output_bits; 		///< number of bits to represent output

	/**
		\brief transition table (stored on the device)
		Table entry format - same as in automaton_host
	*/
	uint32_t* table; 	

	/**
		\brief creates representation of automaton on CUDA device
		\param host_automaton representation of automaton in host memory
		\warning Errors will be checked by assert-like statement
	*/
	automaton_CUDA(const common_auto::automaton_host& host_automaton);

	/**
		will free up allocated memory	
		\warning Errors will be checked by assert-like statement
	*/
	~automaton_CUDA();
private:
	/**
		\brief prohibit copy constructor
	*/
	automaton_CUDA(const automaton_CUDA& other);
	automaton_CUDA& operator=(const automaton_CUDA& other);
} automaton_CUDA;

}