/**
	\file cuda_run_automaton.h
	\brief declares functions that simulate automaton on word complect
*/

#pragma once

#include <cuda_runtime.h>				// cudaError
#include "constants.h"
#include "automaton_cuda.h"				// automaton_CUDA declaration
#include "automaton_compact_cuda.h"		// automaton_compact_CUDA declaration

// TODO - why functions return cudaError?

namespace cuda_auto
{
//#define KERNEL_OUTPUT_INFO // TODO: rename 

// TODO: comments on functions

cudaError run_automaton_compresed_words_concatenated_int(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
);

/**
	\brief simulate automaton on interleaved words
	\param automaton representation on the device
	\param word_count number of words in word complect
	\param word_length number of uint32_t "units" in each word
	\param input pointer to input words (on host)
	\param output pointer to output words (on host)
	\param THREADS_PER_BLOCK number of threads in CUDA thread-block

	Input and output symbols will be compressed

	// TODO - give reference to kv_darbs
	Input and output words are 8-symbol, 32-bit interleaved 
	(8 symbols of 0 word, 8 symbols of 1 word ... 8 symbols of  word_count-1 word),
	(8 symbols of 0 word, 8 symbols of 1 word ... 8 symbols of  word_count-1 word),
	...

	Note that size of input and output allocated memory will be
	word_count*word_length*sizeof(uint32_t)

	Note that each input and output word will contain 8*length symbols,
	symbols in each 32-bit unit will be stored in following way:
*/
cudaError run_automaton_words_interleaved_uint32(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
);

cudaError run_automaton_interleaved_pattern_uint32(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output//,
	//const int THREADS_PER_BLOCK
);

/* Bigger automaton section */

/**
	\brief input and output words will be interleaved
*/
cudaError run_square_automaton(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
);

/* End of bigger automaton section */

/* Dummy section */

cudaError run_automaton_only_automaton_access_dummy(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint8_t* input,
	uint8_t* output,
	const int THREADS_PER_BLOCK
);


/* End of dummy section */


/* compact automaton section */

cudaError run_compact_automaton_words_interleaved_uint32(
	const automaton_compact_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
);

/* End of compact automaton section */

}