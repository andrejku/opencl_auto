#pragma once

#include "cuda_run_automaton.h"		// function declarations
#include "random_functions.h"		// ceiling
#include "cuda_helper.h"			// cuda_assert
#include <cuda_runtime.h>
#include <cstdint>					// uintx_t (uint8_t, uint32_t, ...)
#include <cassert>					// assert

using common_auto::automaton_host;
using common_auto::ceiling;
//using common_auto::log2_floor;
using common_auto::time_diff;

namespace cuda_auto
{

__global__ void run_automaton_compressed_int_ugly_kernel(
	const uint32_t* table,
	int word_count,
	int word_length,
	int state_bits,
	const uint32_t* input,
	uint32_t* output
)
{
	int global_id=blockDim.x*blockIdx.x + threadIdx.x;
	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint32_t input_fetched;
	uint32_t output_combined;
	uint8_t input_small;
	uint8_t output_small;

	if(global_id < word_count)
	{
		input+=global_id*word_length;
		output+=global_id*word_length;
		
		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input;
			output_combined=0U;

			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*j)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*j);
			}

			*output=output_combined;
			input++;
			output++;
		}
	}
}

__global__ void run_automaton_words_interleaved_uint32_kernel(
	const uint32_t* table,
	int word_count,
	int word_length,
	int state_bits,
	const uint32_t* input,
	uint32_t* output
)
{
	int global_id=blockDim.x*blockIdx.x + threadIdx.x;
	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint32_t input_fetched;
	uint8_t input_small;
	uint8_t output_small;
	uint32_t output_combined;

	if(global_id < word_count)
	{
		input+=global_id;
		output+=global_id;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input;
			output_combined=0U;

			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*j)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*j);
			}

			*output=output_combined;
			input+=word_count;
			output+=word_count;
		}
	}
}

cudaError run_automaton_compresed_words_concatenated_int(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint32_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_length*word_count*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);
	cuda_assert(input_copy_error);
	
	dim3 block=dim3(THREADS_PER_BLOCK, 1, 1);
	dim3 grid=dim3(ceiling(word_count, THREADS_PER_BLOCK), 1, 1);

	//other_time+=time_diff_ms();
#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	run_automaton_compressed_int_ugly_kernel
		<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		dev_input,
		dev_output
	);

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_length*word_count*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);
	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);


#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << "\n--------------------\n";
	std::cout << "CUDA concatenated words: " << kernel_time << ' ' << other_time;
	std::cout << "\n--------------------\n";
#endif

	return cudaError::cudaSuccess; // ???
}

cudaError run_automaton_words_interleaved_uint32(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint32_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_count*word_length*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);

	cuda_assert(input_copy_error);
	
	dim3 block=dim3(THREADS_PER_BLOCK, 1, 1);
	dim3 grid=dim3(ceiling(word_count, THREADS_PER_BLOCK), 1, 1);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	run_automaton_words_interleaved_uint32_kernel
		<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		dev_input,
		dev_output
	);

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_count*word_length*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);

	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << "\n--------------------\n";
	std::cout << "CUDA interleaved words: " << kernel_time << ' ' << other_time;
	std::cout << "\n--------------------\n";
#endif

	return cudaError::cudaSuccess; // ???
}

const int BLOCK_SIZE=32;
//const int LOG2_BLOCK_SIZE=log2_floor(BLOCK_SIZE);

// asuming that words are concatenated
#define i_th_word_j_th_unit(words, word_length, i, j) (words)[ (i) * (word_length) + (j) ] 

// a not-so correct but working minimum macro
#define my_min(a,b) (a) < (b) ? (a) : (b)

__global__ void interleaved_pattern_kernel(
	const uint32_t* table,
	int word_count,
	int word_length,
	int state_bits,
	const uint32_t* input,
	uint32_t* output
)
{
	//int global_id=blockDim.x*blockIdx.x + threadIdx.x;
	int local_id=threadIdx.x;
	int group_offset=blockDim.x*blockIdx.x;
	int steps_to_make=my_min(BLOCK_SIZE, word_count-group_offset); 

	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint32_t input_fetched;
	uint8_t input_small;
	uint8_t output_small;
	uint32_t output_combined;

	__shared__ uint32_t input_and_output[BLOCK_SIZE][BLOCK_SIZE];

	for(int current=0; current<word_length; current+=BLOCK_SIZE)
	{
		int thread_symbol=current+local_id;
		bool thread_loads_memory=thread_symbol < word_length;

		
		// collaborative load phase		
		if(thread_loads_memory)
		{
			for(int j=0; j<steps_to_make; j++)
			{
				int current_word = group_offset+j;
				input_and_output[j][local_id]=
					i_th_word_j_th_unit(input, word_length, current_word, thread_symbol);
			}
		}
		
		__syncthreads(); // make sure, that symbols have been loaded
		for(int j=0; j<BLOCK_SIZE; j++)
		{
			input_fetched=input_and_output[local_id][j];
			output_combined=0U;
			
			// calculation phase
			// note that if thread calculates automaton using 0U as input_fetched,
			// than either it calculates it for word with index >= word_count (so no writes there)
			// or for symbol unit with index >= word_length (OK, because we don't need end state of transducer)
			#pragma unroll PACKED_SYMBOLS_PER_UINT32
			for(int jj=0; jj<PACKED_SYMBOLS_PER_UINT32; jj++)
			{
				input_small=(input_fetched >> (SHIFT_BITS*jj)) & SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SHIFT_BITS*jj);
			}

			input_and_output[local_id][j]=output_combined;
		}

		__syncthreads(); // make sure that output was calculated

		// collaborative store phase
		if(thread_loads_memory)
		{
			for(int j=0; j<steps_to_make; j++)
			{
				int current_word = group_offset+j;
				i_th_word_j_th_unit(output, word_length, current_word, thread_symbol)=input_and_output[j][local_id];
			}
		}
		
	}
}

cudaError run_automaton_interleaved_pattern_uint32(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output//,
	//const int THREADS_PER_BLOCK
)
{
	assert(
		automaton.input_bits <= SHIFT_BITS && 
		automaton.output_bits <=SHIFT_BITS
	);

	#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint32_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_count*word_length*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);

	cuda_assert(input_copy_error);
	
	dim3 block=dim3(BLOCK_SIZE, 1, 1);
	dim3 grid=dim3(ceiling(word_count, BLOCK_SIZE), 1, 1);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	interleaved_pattern_kernel<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		dev_input,
		dev_output
	);
	//*/

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_count*word_length*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);

	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << "\n--------------------\n";
	std::cout << "CUDA, interleaved memory access pattern: " << kernel_time << ' ' << other_time;
	std::cout << "\n--------------------\n";
#endif

	return cudaError::cudaSuccess; // ???
}

__global__ void square_automaton_kernel(
	const uint32_t* table,
	int word_count,
	int word_length,
	int state_bits,
	const uint32_t* input,
	uint32_t* output
)
{
	int global_id=blockDim.x*blockIdx.x + threadIdx.x;
	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint32_t input_fetched;
	uint8_t input_small;
	uint8_t output_small;
	uint32_t output_combined;

	if(global_id < word_count)
	{
		input+=global_id;
		output+=global_id;

		for(int i=0; i<word_length; i++)
		{
			input_fetched=*input;
			output_combined=0U;

			#pragma unroll SQUARE_PACKED_SYMBOLS_PER_UINT32
			for(int j=0; j<SQUARE_PACKED_SYMBOLS_PER_UINT32; j++)
			{
				input_small=(input_fetched >> (SQUARE_SHIFT_BITS*j)) & SQUARE_SHIFT_MASK;
				state = table[ (input_small << state_bits) | state ];
				output_small=state >> state_bits;
				state &=state_mask;
				output_combined|=output_small << (SQUARE_SHIFT_BITS*j);
			}

			*output=output_combined;
			input+=word_count;
			output+=word_count;
		}
	}
}

cudaError run_square_automaton(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint32_t* input,
	uint32_t* output,
	const int THREADS_PER_BLOCK
)
{
	assert(
		automaton.input_bits <= SQUARE_SHIFT_BITS && 
		automaton.output_bits <=SQUARE_SHIFT_BITS
	);

#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint32_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_count*word_length*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);

	cuda_assert(input_copy_error);
	
	dim3 block=dim3(THREADS_PER_BLOCK, 1, 1);
	dim3 grid=dim3(ceiling(word_count, THREADS_PER_BLOCK), 1, 1);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	square_automaton_kernel
		<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		dev_input,
		dev_output
	);

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

	//cudaError add_error=cudaDeviceSynchronize();
	//cuda_assert(add_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_count*word_length*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);

	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << "\n--------------------\n";
	std::cout << "CUDA square automaton, interleaved words: " << kernel_time << ' ' << other_time;
	std::cout << "\n--------------------\n";
#endif

	return cudaError::cudaSuccess; // ???
}

}

