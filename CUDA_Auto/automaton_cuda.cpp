#pragma once

#include "automaton_cuda.h"	// automaton_CUDA declaration
#include <cuda_runtime.h>	// cudaMalloc, cudaMemcpy, cudaError
#include "cuda_helper.h"	// cuda_assert
#include <cassert>

using common_auto::automaton_host;

namespace cuda_auto
{

automaton_CUDA::automaton_CUDA(
	const common_auto::automaton_host& host_automaton
)
{
	state_bits=host_automaton.state_bits;
	input_bits=host_automaton.input_bits;
	output_bits=host_automaton.output_bits;

	cudaError table_alloc_error = cudaMalloc(
		(void**) &(table),
		host_automaton.table_size*sizeof(*table)
	);
	cuda_assert(table_alloc_error);

	cudaError table_copy_error = cudaMemcpy(
		(void*) table,
		(void*) host_automaton.table,
		host_automaton.table_size*sizeof(*table),
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);
	cuda_assert(table_copy_error);
}

automaton_CUDA::~automaton_CUDA()
{
	cudaError table_free_error = cudaFree((void*) table);
	cuda_assert(table_free_error);
}

}