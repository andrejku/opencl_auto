#include "cuda_run_automaton.h"		// function declarations
#include "random_functions.h"		// ceiling
#include "cuda_helper.h"			// cuda_assert
#include <cstdint>					// intx_t

using common_auto::ceiling;
using common_auto::time_diff;

namespace cuda_auto
{

__global__ void run_automaton_only_automaton_access_dummy_kernel(
	const uint32_t* table,
	int word_count,
	int word_length,
	int state_bits,
	int input_bits,
	const uint8_t* input,
	uint8_t* output
)
{
	int global_id=blockDim.x*blockIdx.x + threadIdx.x;

	uint32_t state_mask = (1U<<state_bits)-1U;
	uint32_t state=0U;

	uint8_t dummy_input;
	uint8_t dummy_result=0;

	uint32_t rand_0=global_id+5U;
	uint32_t rand_1=global_id*global_id+1U;
	uint32_t rand_2;
	uint32_t input_mask=1U<<input_bits-1U;

	int full_len=word_length>>2; // divide by 4, sizeof(uint32_t)/sizeof(uint8_t)

	if(global_id < word_count)
	{
		for(int i=0; i<full_len; i++)
		{
			rand_2=rand_1*rand_1+rand_0+1U;
			rand_0=rand_1;
			rand_1=rand_2;

			dummy_input=rand_2&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>4)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>8)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>16)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>20)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>24)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;

			dummy_input=(rand_2>>28)&input_mask;
			state=table[( dummy_input << state_bits) | state];
			state &= state_mask;
		}

		dummy_result=state;
		output[global_id]=dummy_result; // Do not want compiler to optimize memory accesses away
	}
}

cudaError run_automaton_only_automaton_access_dummy(
	const automaton_CUDA& automaton,
	int word_count,
	int word_length,
	const uint8_t* input,
	uint8_t* output,
	const int THREADS_PER_BLOCK
)
{
#ifdef KERNEL_OUTPUT_INFO
	int kernel_time=0;
	int other_time=0;
	time_diff();
#endif

	uint8_t *dev_input, *dev_output;
	
	cudaError input_alloc_error = cudaMalloc(
		(void**) &dev_input,
		word_count*word_length*sizeof(*dev_input)
	);
	cuda_assert(input_alloc_error);

	cudaError output_alloc_error = cudaMalloc(
		(void**) &dev_output,
		word_count*word_length*sizeof(*dev_output)
	);
	cuda_assert(output_alloc_error);

	cudaError input_copy_error = cudaMemcpy(
		(void*) dev_input, 
		(const void*) input, 
		word_count*word_length*sizeof(*dev_input), 
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);

	cuda_assert(input_copy_error);
	
	dim3 block=dim3(THREADS_PER_BLOCK, 1, 1);
	dim3 grid=dim3(ceiling(word_count, THREADS_PER_BLOCK), 1, 1);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
#endif

	run_automaton_only_automaton_access_dummy_kernel<<<grid, block>>> (
		automaton.table,
		word_count,
		word_length,
		automaton.state_bits,
		automaton.input_bits,
		dev_input,
		dev_output
	);

	cudaError kernel_error = cudaPeekAtLastError();
	cuda_assert(kernel_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	kernel_time=time_diff();
#endif

	cudaError output_copy_error = cudaMemcpy(
		(void*) output,
		(const void*) dev_output,
		word_count*word_length*sizeof(*dev_output),
		cudaMemcpyKind::cudaMemcpyDeviceToHost
	);

	cuda_assert(output_copy_error);

	cudaError input_free_error = cudaFree(dev_input);
	cudaError output_free_error = cudaFree(dev_output);
	cuda_assert(input_free_error);
	cuda_assert(output_free_error);

#ifdef KERNEL_OUTPUT_INFO
	cudaDeviceSynchronize();
	other_time+=time_diff();
	std::cout << kernel_time << ' ' << other_time << '\n';
#endif

	return cudaError::cudaSuccess; // ???
}

}