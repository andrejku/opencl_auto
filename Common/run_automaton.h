/**
	\file run_automaton.h
	\brief automaton simulation on CPU
*/

#pragma once
#include "automaton_host.h"	// automaton_host class

namespace common_auto
{

// TODO - better comment

/**
	\brief simulates automaton on a given word
	\param A automaton to be simulated
	\param input pointer to uncompressed input word
	\param length number of uint8_t "units" in word
	\param state start state of automaton, defaults to 0

	Input symbols are stored as lower bits of each uint8_t unit
*/
void run_automaton(
	const automaton_host &A,
	const uint8_t* input, 
	int length,
	uint8_t* output,
	uint32_t state=0U
)
{
	for(int i = 0; i < length; i++)
	{
		state = A.table[(input[i] << A.state_bits) | state];
		output[i] = state >> A.state_bits;
		state &= A.state_mask;
	}
}

}