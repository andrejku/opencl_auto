/**
	\file automaton_host.h
	\brief Definition of automaton_host class

	In this file class automaton_host is defined, 
	This class is a building block for CUDA (automaton_CUDA) 
	and OpenCL (automaton_CL) automaton structures.
*/

#pragma once		

#include <cstdint>	// intx_t

namespace common_auto 
{

/** 
	\brief Class that holds representation of automaton (DFA, transducer) in host memory

	A structure to represent finite state machine (DFA, finite state transducer).
	Automaton is stored as a lookup table.
	For each combination of input character and state
	this table stores output character and next state.
*/
class automaton_host
{
public: 
	int state_bits; 	///< number of bits to represent state of automaton 
	int input_bits; 	///< number of bits to represent input symbol
	int output_bits; 	///< number of bits to represent output symbol

	/**
		\brief number of table entries 
		(will be equal to 2^(input_bits+state_bits)) 
	*/
	int table_size;

	/** 
		\brief number of states that the automaton has
		(will be equal to 2^state_bits
	*/
	int state_count; 	

	/** 
		\brief mask to get state bits 
		(will be equal to 2^state_bits-1)
	*/
	uint32_t state_mask;

	// TODO: clearer comment
	/** 
		\brief transition table
		each entry will store next state in lower bits [0:state_bits-1] 
		and output character in higher bits [state_bits: state_bits+input_bits-1]

		Table is stored in row-major order, where rows correspond to
		input symbols and columns - to current state
	*/
	uint32_t * table; 	

	/**
		\brief constructs automaton with given parameter sizes
		\param _state_bits number of bits to represent states of automaton
		\param _input_bits number of bits to represent input symbol
		\param _output_bits number of bits to represent output symbol

		Allocates table with appropriate size
		\warning Errors will be checked by assert-like statement
	*/
	automaton_host(int _state_bits, int _input_bits, int _output_bits);

	/**
		\brief frees up memory allocated to automaton table
	*/
	~automaton_host();

	// TODO - RMV? if test projects will be separated, this will belong in test_common
	/**
		\brief randomly fill table of automaton
		\param set_rand whether to reset seed of random number generator
	*/
	void rand_fill(bool set_rand = false);		

	// TODO - it is possible to calculate squared automaton on the device
	// (saving host time AND memory)
	/**
		\brief create square of the given automaton
		\param A automaton instance, square of which will be returned

		The resulting alphabet; TODO - comment
	*/
	//static automaton_host square_automaton(const automaton_host& A);
	static void square_automaton(const automaton_host& A, automaton_host& A2);
	//void square_automaton(const automaton_host& A);
private:
	/**
		\brief calculate auxiliary automaton parameters
	*/
	void calculate_sizes(void);
	
	/**
		\brief prohibit copy constructor

		automaton_host class, 
		as well as automaton_CUDA, automaton_CL (and other) structs
		do not have a copy constructor, because

		- deep copying table (specially on GPU device) could be expensive
		- shallow copying could be dangerous once one of the instances 
		  that shares pointer to table gets destroyed
	*/
	automaton_host(const automaton_host& other);
	automaton_host& operator=(const automaton_host& other);
}; 

}