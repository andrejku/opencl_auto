#pragma once

#include "random_functions.h" // function declarations

#include <cstdlib>	// rand, srand
#include <cassert>	// assert
#include <ctime>	// time
#include <chrono>	// std::chrono::high_resolution

#include <random>

// TODO: random number generation looks ugly

namespace common_auto
{

/*
// log2(n) rounded down
int log2_floor(unsigned int n) 
{
	int result=0;
	
	while(n > 1)
	{
		n >>= 1;
		result++;
	}

	return result;
}

// should be at least 15
const static int RAND_GENERATED_BITS=log2_floor(RAND_MAX + 1);
*/

std::mt19937 gen;
bool gen_initialized=false;

void init_gen(void)
{
	if(!gen_initialized)
	{
		std::random_device rd;
		gen=std::mt19937(rd());
		gen_initialized=true;
	}
}

std::uniform_int_distribution<uint32_t> distribution_automaton(0, (1<<24)-1);
std::uniform_int_distribution<uint32_t> distribution_words(0, (1<<8)-1);

unsigned int bigger_rand(void)
{
	init_gen();
	return distribution_automaton(gen);
}


// fill array of characters with random values (from 0 up to 2^char_bits-1)
void rand_char(
	unsigned char *result,
	size_t char_bits, 
	size_t length,
	bool set_rand
)
{
	// char_bits exceeded size of unsigned char
	assert(char_bits <= CHAR_BIT*sizeof(unsigned char));
	unsigned char mask = (1U << char_bits) - 1U;
	init_gen();

	for(size_t i = 0; i < length; i++)
	{
		result[i]=distribution_words(gen) & mask;
	}
}

int time_diff(void)
{
	static decltype(std::chrono::high_resolution_clock::now()) prev_t;
	auto curr_t=std::chrono::high_resolution_clock::now();

	int result=(int)std::chrono::duration_cast
		<std::chrono::microseconds>(curr_t-prev_t).count();
	prev_t=curr_t;
	
	return result;
}

int time_diff_ms(void)
{
	static decltype(std::chrono::high_resolution_clock::now()) prev_t;
	auto curr_t=std::chrono::high_resolution_clock::now();

	int result=(int)std::chrono::duration_cast
		<std::chrono::milliseconds>(curr_t-prev_t).count();
	prev_t=curr_t;
	
	return result;
}

int ceiling(int a, int b)
{
	return (a+b-1)/b;
}

}