// RMV? some functions are only needed in testing
#pragma once

namespace common_auto
{
//int log2_floor(unsigned int n);

// generates 2*ord2(RAND_MAX+1) random bits (should be at least 30) -- TODO - wrong comment
unsigned int bigger_rand(void);

// fills memory block with random values 
void rand_char(
	unsigned char *result, 
	size_t char_bits, 
	size_t length, 
	bool set_rand=false
);

// TODO - rename function to time_diff_mks
// measure time between previous launch of this function and now (in mks) 
int time_diff(void);

// measure time between previous launch of this function and now (in ms)
int time_diff_ms(void);

// returns ceiling (value rounded up) from division of a/b
int ceiling(int a, int b);

}