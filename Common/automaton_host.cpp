#pragma once
#include <cstddef>					// NULL
#include <cassert>					// assert
#include <ctime>					// time

//#include <cstring>					// memcpy
//#pragma warning (disable : 4996)	// adding this to use (deprecated) fopen with Microsoft compiler
//#include <stdio.h>					// fopen, fclose, fscanf, fprintf

#include "automaton_host.h"			// automaton_host class
#include "random_functions.h"		// bigger_rand
#include <new>						// std::nothrow
#include <random>

namespace common_auto
{

automaton_host::automaton_host(
	int _state_bits, 
	int _input_bits, 
	int _output_bits
) :  
	state_bits(_state_bits), 
	input_bits(_input_bits), 
	output_bits(_output_bits), 
	table(NULL)
{
	calculate_sizes();
	bool data_fits_in_uint32=state_bits+output_bits<=CHAR_BIT*sizeof(*table); 
	assert(data_fits_in_uint32);

	table=new (std::nothrow) uint32_t [table_size];
	assert(table!=NULL);
}

automaton_host::~automaton_host() 
{
	delete[] table;
}

void automaton_host::rand_fill(bool set_rand)
{
	// mask to get exactly output_bits+state_bits
	unsigned int mask = (1U << (output_bits + state_bits)) - 1U; 
	
	for(int i = 0; i < table_size; i++)
	{
		table[i] = bigger_rand() & mask;
	}
}


void automaton_host::calculate_sizes(void)
{
	table_size = 1 << (state_bits + input_bits );
	state_count =  1 << state_bits;
	state_mask = (1U << state_bits) - 1U; 
}

//automaton_host automaton_host::square_automaton(const automaton_host& A)
void automaton_host::square_automaton(const automaton_host& A, automaton_host& A2)
{
	//automaton_host R(A.state_bits, 2*A.input_bits, 2*A.output_bits);
	assert(A.state_bits==A2.state_bits && 2*A.input_bits==A2.input_bits && 2*A.output_bits==A2.output_bits);

	uint32_t sigma_in_size=1<<A.input_bits;
	for(uint32_t i1=0U; i1<sigma_in_size; i1++)
	{
		for(uint32_t i2=0U; i2<sigma_in_size; i2++)
		{
			for(uint32_t s=0U; s<uint32_t(A.state_count); s++)
			{
				uint32_t s1=A.table[(i1<<A.state_bits) | s];
				uint32_t o1=s1 >> A.state_bits;
				s1&=A.state_mask;

				uint32_t s2=A.table[(i2<<A.state_bits) | s1];
				uint32_t o2=s2 >> A.state_bits;
				s2&=A.state_mask;

				uint32_t input = (i2 << A.input_bits) | i1;
				uint32_t output = (o2 << A.output_bits) | o1;

				A2.table[(input << A.state_bits) | s ] = 
					(output << A.state_bits) | s2;
			}
		}
	}

	//return R;
	return;
}

}