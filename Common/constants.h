#include <cstdint>
#include <climits>

// could not feed a const int into CUDA #pragma directive... 
/*
const int SHIFT_BITS=4; // assumed max number of bits in symbol in most automaton simulations
const uint32_t SHIFT_MASK=(1U<<SHIFT_BITS)-1U; // mask to get lower SHIFT_BITS bits
const int PACKED_SYMBOLS_PER_UINT8=(CHAR_BIT*sizeof(uint8_t))/SHIFT_BITS;
const int PACKED_SYMBOLS_PER_UINT32=(CHAR_BIT*sizeof(uint32_t))/SHIFT_BITS;

const int SQUARE_SHIFT_BITS=2*SHIFT_BITS;
const uint32_t SQUARE_SHIFT_MASK=(1U<<SQUARE_SHIFT_BITS)-1U;
const int SQUARE_PACKED_SYMBOLS_PER_UINT32=(CHAR_BIT*sizeof(uint32_t))/SQUARE_SHIFT_BITS;
*/

#define SHIFT_BITS 4	// assumed max number of bits in symbol in most automaton simulations
#define SHIFT_MASK 0xFU	// 1<<SHIFT_BITS-1, mask to get lower SHIFT_BITS bits
#define PACKED_SYMBOLS_PER_UINT8 2 
#define PACKED_SYMBOLS_PER_UINT32 8

#define SQUARE_SHIFT_BITS 8 // when running square automaton
#define SQUARE_SHIFT_MASK 0xFFU
#define SQUARE_PACKED_SYMBOLS_PER_UINT32 4 