/**
	\file launch_definitions.h
	\brief "bridge" between testing functions and automaton execution

	Functions that are implemented in this file are used to "hide"
	possible differences between different automaton execution algorithms.

	Note that functions in this header have similar signatures:
	1 arg - automaton_data (or inherited) class
	2 arg - io_data (or inherited) class
	3 arg - test_params struct
*/

#pragma once

#include "io_data.h"
#include "io_data_compressed.h"
#include "io_data_concatenated_compressed.h"
#include "io_data_compressed_interleaved.h"

#include "automaton_data.h"
#include "cuda_automaton_data.h"
#include "cl_automaton_data.h"

#include "cuda_compact_automaton_data.h"

#include "io_data_pinned.h"
#include "cl_automaton_texture_data.h"

#include "run_automaton.h"
#include "cuda_run_automaton.h"
#include "cl_run_automaton.h"

#include <cstdint>

using namespace common_auto;
using namespace cuda_auto;
using namespace cl_auto;

void launch_automaton_native_correct(
	const automaton_data& automaton_data,
	io_data& words_data,
	const test_params& p
)
{
	for(int i=0; i<words_data.p.word_count; i++)
	{
		run_automaton(
			automaton_data.host_automaton,
			words_data.input[i],
			words_data.p.word_length,
			words_data.output_regular[i],
			0U
		);
	}
}

void launch_cuda_only_automaton_access_dummy(
	const cuda_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	run_automaton_only_automaton_access_dummy(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		//p.p_words.word_length,
		words_data.compressed_word_length,
		words_data.input_concatenated_compressed,
		words_data.output_concatenated_compressed,
		p.threads
	);
}

const static int INT_CHAR_RATIO=sizeof(uint32_t)/sizeof(uint8_t);

void launch_cuda_compressed_words_concatenated_int(
	const cuda_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	run_automaton_compresed_words_concatenated_int(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		words_data.compressed_word_length/INT_CHAR_RATIO,
		(uint32_t*) words_data.input_concatenated_compressed,
		(uint32_t*) words_data.output_concatenated_compressed,
		p.threads
	);
}

void launch_cuda_words_interleaved_uint32(
	const cuda_automaton_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	run_automaton_words_interleaved_uint32(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cuda_interleaved_pattern(
	const cuda_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	run_automaton_interleaved_pattern_uint32(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		words_data.compressed_word_length/INT_CHAR_RATIO,
		(uint32_t*) words_data.input_concatenated_compressed,
		(uint32_t*) words_data.output_concatenated_compressed
	);
}

void launch_cuda_square_automaton(
	const cuda_automaton_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	run_square_automaton(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cuda_compact_words_interleaved_uint32(
	const cuda_compact_automaton_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	run_compact_automaton_words_interleaved_uint32(
		automaton_data.cuda_automaton,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cl_words_concatenated_compressed_int(
	const cl_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	cl_run_automaton_words_concatenated_compressed_int(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.compressed_word_length/INT_CHAR_RATIO,
		(uint32_t*) words_data.input_concatenated_compressed,
		(uint32_t*) words_data.output_concatenated_compressed,
		p.threads
	);
}

void launch_cl_interleaved_uint32(
	const cl_automaton_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	cl_run_automaton_symbols_interleaved_uint32(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cl_interleaved_pattern_uint32(
	const cl_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	cl_run_automaton_words_concatenated_interleaved_access(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.compressed_word_length/INT_CHAR_RATIO,
		(uint32_t*) words_data.input_concatenated_compressed,
		(uint32_t*) words_data.output_concatenated_compressed
	);
}

void launch_cl_square_automaton(
	const cl_automaton_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	// running with io_data_compressed_interleaved will give
	// correct results only with these parameters!
	assert(p.p_auto.input_bits==4 &&p.p_auto.output_bits==4);
	
	cl_run_square_automaton(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cl_pinned_interleaved(
	const cl_automaton_data& automaton_data,
	io_data_pinned_interleaved_cl& words_data,
	const test_params& p
)
{
	cl_run_pinned_words(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.T_length,
		words_data.pinned_words,
		p.threads
	);
}

void launch_cl_texture(
	const cl_automaton_texture_data& automaton_data,
	io_data_compressed_interleaved<uint32_t>& words_data,
	const test_params& p
)
{
	cl_run_texture_automaton(
		automaton_data.cl_automaton_texture,
		*p.kernel,
		words_data.p.word_count,
		words_data.T_length,
		words_data.input_compressed_interleaved,
		words_data.output_compressed_interleaved,
		p.threads
	);
}

void launch_cl_only_automaton_access_dummy(
	const cl_automaton_data& automaton_data,
	io_data_concatenated_compressed& words_data,
	const test_params& p
)
{
	cl_only_automaton_access_dummy(
		automaton_data.cl_automaton,
		*p.kernel,
		words_data.p.word_count,
		words_data.compressed_word_length,
		words_data.input_concatenated_compressed,
		words_data.output_concatenated_compressed,
		p.threads
	);
}