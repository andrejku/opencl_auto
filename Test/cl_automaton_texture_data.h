
#pragma once
#include "automaton_texture_CL.h"
#include "automaton_data.h"
#include "test_params.h"
#include "CL_helper.h"

using namespace cl_auto;

// TODO: move implementation to .cpp

class cl_automaton_texture_data
	: public automaton_data
{
public:
	automaton_text_CL cl_automaton_texture; 
	cl_automaton_texture_data(const automaton_params& params)
		:automaton_data(params),
		cl_automaton_texture(host_automaton, *p.cl_context)
	{
		//cl_automaton_text=*create_cl_text_automaton(host_automaton, *p.cl_context);
	}

	~cl_automaton_texture_data()
	{

	}

	void refill_automaton(void)
	{
		automaton_data::refill_automaton();
		cl::CommandQueue queue(*p.cl_context);
		
		cl::size_t<3> origin;
		origin[0]=0;
		origin[1]=0;
		origin[2]=0;

		cl::size_t<3> region;
		region[0]=host_automaton.table_size;
		region[1]=1;
		region[2]=1;

		cl_int table_copy_error=queue.enqueueWriteImage(
			cl_automaton_texture.table,
			CL_FALSE,
			origin,
			region,
			0,
			0,
			(void*) host_automaton.table
		);

		cl_assert(table_copy_error);

		cl_int queue_finish_error = queue.finish();
		cl_assert(queue_finish_error);
	}
private:
	cl_automaton_texture_data(const cl_automaton_texture_data& other); ///< prohibit copy
	cl_automaton_texture_data& operator=(const cl_automaton_texture_data& other);
protected:

};