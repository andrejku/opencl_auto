#pragma once

#include "io_data.h"			// io_data class declaration
#include "test_utility.h"		// alloc_words, delete_words
#include <iostream>				// std::cerr
#include "random_functions.h"	// rand_char

io_data::io_data(const word_params& params):
	p(params),
	word_length_padded(ceiling(params.word_length, PAD_SIZE)*PAD_SIZE)
{
	input = alloc_words<uint8_t>(p.word_count, word_length_padded);
	output = alloc_words<uint8_t>(p.word_count, word_length_padded);
	output_regular = alloc_words<uint8_t>(p.word_count, word_length_padded);
}

io_data::~io_data()
{
	delete_words(input, p.word_count);
	delete_words(output, p.word_count);
	delete_words(output_regular, p.word_count);
}

void io_data::refill_words(void)
{
	for(int i=0; i<p.word_count; i++)
	{
		// io_data has to "know" how many input bits are in each symbol
		rand_char(input[i], p.input_bits, word_length_padded, false);
	}
}

void io_data::prepare_output(void)
{
	; // Nothing
}

bool io_data::check_correct(void)
{
	prepare_output(); // prepare the output in the child class, only then compare
	
	size_t bytes_in_word_unpadded=p.word_length*sizeof(**output);

	for(int i=0; i<p.word_count; i++)
	{
		if(memcmp(output_regular[i], output[i], bytes_in_word_unpadded)!=0)
		{
			std::cerr << "First different word: " << i << std::endl;
			for(int j=0; j<p.word_length; j++)
			{
				if(output_regular[i][j]!=output[i][j])
				{
					std::cerr << 
						"First difference at position: " << j << std::endl;
					break;
				}
			}

			return false;
		}
	}

	return true;
}

int io_data::symbols_processed(void)
{
	return p.word_count*p.word_length;
}