#pragma once

#include "test_correct.h"	// correctness tests
#include "avg_speed.h"		// average speed performance tests
#include <iostream>			// std::cout, std::cin -- TODO?

using std::cout;
using std::endl;
using namespace cl_auto;

void test_memory_cl_pinned_words(void)
{
	const int SIZE=1000*1000*20;
	cl::Context cnt=get_first_GPU_context();

	for(int i=0; i<100; i++)
	{
		cl_pinned_words words(SIZE, cnt);
		words.map_input();
		words.map_output();
	}
}

void test_memory_cl_pinned_words_2(void)
{
	const int SIZE=1000*1000*20;
	cl::Context cnt=get_first_GPU_context();
	cl_pinned_words words(SIZE, cnt);

	for(int i=0; i<100; i++)
	{
		cout << std::hex;
		
		void *input=words.map_input();
		void *output=words.map_output();

		cout << int(input) << endl;
		cout << int(output) << endl;

		cl::CommandQueue q(cnt);

		q.enqueueUnmapMemObject(
			words.input_words_pinned,
			input,
			NULL,
			NULL
		);

		q.enqueueUnmapMemObject(
			words.output_words_pinned,
			output,
			NULL,
			NULL
		);
	}
}

void test_memory_cl_pinned_words_3(void)
{
	const int SIZE=1000*1000*20;
	cl::Context cnt=get_first_GPU_context();
	cout << std::hex;

	for(int i=0; i<100; i++)
	{
		cl_pinned_words words(SIZE, cnt);

		void *input=words.map_input();
		void *output=words.map_output();

		cout << int(input) << endl;
		cout << int(output) << endl;
	}
}

void test_memory_automaton_CUDA(void)
{
	automaton_host A(20,4,4);

	for(int i=0; i<100; i++)
	{
		automaton_CUDA cuda_A(A);
	}
}

void test_memory_automaton_CL_text(void)
{
	automaton_host A(11,4,4);
	cl::Context cnt=get_first_GPU_context();

	for(int i=0; i<100; i++)
	{
		automaton_text_CL cl_A(A, cnt);
	}
}

void test_memory_automaton_CL(void)
{
	automaton_host A(20,4,4);
	cl::Context cnt=get_first_GPU_context();

	for(int i=0; i<100; i++)
	{
		automaton_CL cl_A(A, cnt);
	}
}

void dummy(void)
{
	using std::string;

	automaton_host A(20,4,4);
	A.rand_fill();
	automaton_CUDA A_cuda(A);

	cl::Context cnt=get_first_GPU_context();
	automaton_CL A_cl(A, cnt);

	cl::Kernel concat=create_kernel(cnt, "concat_compressed_int.cl", "run_automaton_compressed_int", "");
	
	cl::Kernel interl=create_kernel(cnt, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");

	cl::Kernel cl_dummy=create_kernel(cnt, "only_automaton_access.cl", "only_automaton_access_dummy", "");

	uint32_t* input=new uint32_t[DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/8];
	uint32_t* input_interleaved=new uint32_t[DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/8];
	uint32_t* output=new uint32_t[DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/8];

	int count=DEFAULT_TEST_WORD_COUNT;
	int len=DEFAULT_TEST_WORD_LENGTH/8;

	//rand_char((uint8_t*)input, 8, DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/2);
	uint8_t *input_8=(uint8_t*) input;
	

	/*

	unsigned char mask = (1U << 4) - 1U;
	for(int i=0; i<DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/2; i++)
	{
		uint8_t lo=rand()&mask;
		uint8_t hi=rand()&mask;
		input_8[i]=(hi<<4)|lo;
	}
	*/
	rand_char(input_8, 8, DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH/2, false);

	for(int i=0; i<count; i++)
	{
		for(int j=0; j<len; j++)
		{
			input_interleaved[j*count+i]=input[i*len+j];
		}
	}

	const int RETRY=10;
	time_diff_ms();

	/*
	for(int i=0; i<RETRY; i++)
	{
		run_automaton_compresed_words_concatenated_int(A_cuda, count, len, input, output, 64);
	}
	int compressed=time_diff_ms();
	*/

	for(int i=0; i<RETRY; i++)
	{
		//run_automaton_words_interleaved_uint32(A_cuda, count, len, input, output, 64);
		run_automaton_words_interleaved_uint32(A_cuda, count, len, input_interleaved, output, 64);
	}
	int interleaved=time_diff_ms();

	for(int i=0; i<RETRY; i++)
	{
		run_automaton_only_automaton_access_dummy(A_cuda, count, len*4, (uint8_t*)input, (uint8_t*)output, 64);
	}
	int dumb=time_diff_ms();

	/*
	for(int i=0; i<RETRY; i++)
	{
		//cl_run_automaton_symbols_interleaved_uint32(A_cl, interl, count, len, input, output, 128);
		cl_run_automaton_symbols_interleaved_uint32(A_cl, interl, count, len, input_interleaved, output, 64);
	}
	int cl_interleaved=time_diff_ms();
	*/

	for(int i=0; i<RETRY; i++)
	{
		cl_only_automaton_access_dummy(A_cl, cl_dummy, count, len*4, (uint8_t*)input, (uint8_t*) output, 64);
	}
	int cl_dumb=time_diff_ms();

	cout << /*compressed << ' ' <<*/ interleaved << ' ' << dumb << ' ' << cl_dumb
		//<< ' ' << cl_interleaved << ' ' << cl_dumb
	<< '\n';

	double char_count=DEFAULT_TEST_WORD_COUNT*DEFAULT_TEST_WORD_LENGTH*RETRY;
	cout << /*char_count/compressed << ' ' <<*/ char_count/interleaved << ' ' << char_count/dumb << ' ' << char_count/cl_dumb
		//<< ' ' << char_count/cl_interleaved << ' ' << char_count/cl_dumb
	;

	delete[] input;
	delete[] input_interleaved;
	delete[] output;
}