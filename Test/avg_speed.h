/**
	\file avg_speed.h
	\brief performance tests that measure average speed of automaton simulation
*/

#pragma once

#include "launch_definitions.h"
#include "random_functions.h"
#include <fstream>				// std::ofstream
#include <iostream>				// std::cout, std::endl

/**
	\brief generic function that measures average speed of simulation
	\tparam T_auto type of automaton_data
	\tparam T_words type of io_data
	\param p structure that holds automaton sizes, word complect sizes and additional info
	\param call_automaton function that simulates automaton
*/
template<typename T_auto, typename T_words>
double avg_speed_generic(
	const test_params& p,
	void (*call_automaton) (const T_auto&, T_words&, const test_params&)
)
{
#if FAST_PERFORMANCE_TEST == 1
	const static int WORD_COMPLECTS_PER_AUTOMATON=1;
	const static int DIFFERENT_AUTOMATONS=5;
#elif FAST_PERFORMANCE_TEST == 2
	const static int WORD_COMPLECTS_PER_AUTOMATON=4;
	const static int DIFFERENT_AUTOMATONS=10;
#else
	const static int WORD_COMPLECTS_PER_AUTOMATON=10;
	const static int DIFFERENT_AUTOMATONS=10;
#endif

	T_auto automaton_data(p.p_auto);
	std::vector<T_words*> words_data;
	for(int w_compl=0; w_compl<WORD_COMPLECTS_PER_AUTOMATON; w_compl++)
	{
		words_data.push_back(new T_words(p.p_words));
	}

	int total_time_ms=0;

	for(int au=0; au<DIFFERENT_AUTOMATONS; au++)
	{
		automaton_data.refill_automaton();
		for(int w_compl=0; w_compl<WORD_COMPLECTS_PER_AUTOMATON; w_compl++)
		{
			words_data[w_compl]->refill_words();
		}
			
		time_diff_ms();
		for(int w_compl=0; w_compl<WORD_COMPLECTS_PER_AUTOMATON; w_compl++)
		{
			call_automaton(automaton_data, *words_data[w_compl], p);
		}

		int curr_time=time_diff_ms();
		total_time_ms+=curr_time;
	}

#if SILENCE==0
	std::cout << "State bits: " << p.p_auto.state_bits << "\n";
	std::cout << "Total elapsed time: " << total_time_ms << "\n";
	std::cout << "Workgroup size: " << p.threads << "\n";
	std::cout << "Word count: " << p.p_words.word_count << '\n';
	std::cout << "Word length: " << p.p_words.word_length << '\n';
#endif

	double total_symbols_processed=words_data[0]->symbols_processed() * 
		double(WORD_COMPLECTS_PER_AUTOMATON) *
		DIFFERENT_AUTOMATONS;

	for(int w_compl=0; w_compl<WORD_COMPLECTS_PER_AUTOMATON; w_compl++)
	{
		delete words_data[w_compl];
	}

	return (total_symbols_processed)/(double(total_time_ms));
}

double avg_speed_cuda_only_automaton_access_dummy(test_params p)
{
	return avg_speed_generic
		<cuda_automaton_data,io_data_concatenated_compressed>
			(p, launch_cuda_only_automaton_access_dummy);
}

double avg_speed_cuda_compressed_words_concatenated_int(test_params p)
{
	return avg_speed_generic
		<cuda_automaton_data,io_data_concatenated_compressed>
			(p, launch_cuda_compressed_words_concatenated_int);
}

double avg_speed_cuda_words_interleaved_uint32(test_params p)
{
	return avg_speed_generic
		<cuda_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cuda_words_interleaved_uint32);
}

double avg_speed_cuda_interleaved_pattern(test_params p)
{
	return avg_speed_generic
		<cuda_automaton_data, io_data_concatenated_compressed>
			(p, launch_cuda_interleaved_pattern);
}

double avg_speed_cuda_square_automaton(test_params p)
{
	if(p.p_auto.state_bits+2*p.p_auto.input_bits>=28) return 0.0; // 1 GB,
	p.p_auto.square_automaton=true;

	return avg_speed_generic
		<cuda_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cuda_square_automaton);
}

double avg_speed_cuda_compact_words_interleaved_uint32(test_params p)
{
	if(p.p_auto.output_bits+p.p_auto.state_bits>CHAR_BIT*sizeof(uint16_t))
	{
		return 0.0;
	}
	else 
	{
		return avg_speed_generic
		<cuda_compact_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cuda_compact_words_interleaved_uint32);
	}
}

double avg_speed_cl_words_concatenated_compressed_int(test_params p)
{
	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "concat_compressed_int.cl", "run_automaton_compressed_int", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_concatenated_compressed>
			(p, launch_cl_words_concatenated_compressed_int);
}

double avg_speed_cl_interleaved_uint32(test_params p)
{
	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cl_interleaved_uint32);
}

double avg_speed_cl_interleaved_pattern(test_params p)
{
	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "interleaved_pattern.cl", "run_automaton_interleaved_pattern", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_concatenated_compressed>
			(p, launch_cl_words_concatenated_compressed_int);
}

double avg_speed_cl_square_automaton(test_params p)
{
	if(p.p_auto.state_bits+2*p.p_auto.input_bits>=28) return 0.0; // 1 GB,
	p.p_auto.square_automaton=true;

	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "square_automaton.cl", "run_square_automaton_uint32", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cl_square_automaton);
}


double avg_speed_cl_pinned_interleaved_uint32(test_params p)
{
	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_pinned_interleaved_cl>
			(p, launch_cl_pinned_interleaved);
}

double avg_speed_cl_texture(test_params p)
{
	if(p.p_auto.state_bits+p.p_auto.input_bits >= 12 + 4) return 0.0;

	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "texture_interleaved_uint32.cl", "run_automaton_texture_words_interleaved_uint32", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_texture_data, io_data_compressed_interleaved<uint32_t>>
			(p, launch_cl_texture);
}

double avg_speed_cl_only_automaton_access_dummy(test_params p)
{
	cl::Kernel kernel=create_kernel(*p.p_auto.cl_context, "only_automaton_access.cl", "only_automaton_access_dummy", "");
	p.kernel=&kernel;

	return avg_speed_generic
		<cl_automaton_data, io_data_concatenated_compressed>
			(p, launch_cl_only_automaton_access_dummy);
}

double avg_speed_run_automaton_cpu(test_params p)
{
	return avg_speed_generic
		<automaton_data, io_data>(p, launch_automaton_native_correct);
}

std::vector<double> avg_speed_v(
	std::vector<test_params> params,
	double (*avg_speed_function) (test_params)
)
{
	std::vector<double> result;
	for(auto &param: params)
	{
		result.push_back(avg_speed_function(param));
	}
	return result;
}

template<typename T>
std::vector<double> convert_to_double_vector(const std::vector<T>& v)
{
	std::vector<double> result;

	for(const auto& x: v)
	{
		result.push_back(double(x));
	}

	return result;
}

void output_vector_to_file(
	const std::string& filename, 
	const std::vector<std::vector<double>> &all_results
)
{
	std::ofstream out(filename.c_str());

	for(const auto& v: all_results)
	{
		for(const auto& x: v)
		{
			out << x << ' ';
		}
		out << std::endl;
	}

	out.close();
}

std::ostream& operator << (std::ostream& out, const std::vector<double> &v)
{
	for(const auto& x: v)
	{
		out << x << ' ';
	}
	return out;
}

const int INPUT_OUTPUT_BITS=4;

const int OPENCL_DEFAULT_THREADS_PER_BLOCK=64;
const int CUDA_DEFAULT_THREADS_PER_BLOCK=64;
const int CPU_DEFAULT_THREADS_PER_BLOCK=4;

const int DEFAULT_TEST_WORD_COUNT=1024;
const int DEFAULT_TEST_WORD_LENGTH=4096;

test_params default_test_params(
	int state_bits, 
	cl::Context* context,
	int threads = OPENCL_DEFAULT_THREADS_PER_BLOCK
)
{
	return test_params(
		state_bits,
		INPUT_OUTPUT_BITS,
		INPUT_OUTPUT_BITS,
		DEFAULT_TEST_WORD_COUNT,
		DEFAULT_TEST_WORD_LENGTH,
		threads,
		context,
		NULL
	);
}

using std::vector;
const std::string python_path="../Python/";

std::vector<int> automaton_sizes_1(void)
{
	std::vector<int> result;
	//for(int i=1; i<=20; i++) result.push_back(i);
	for(int i=20; i>=1; i--) result.push_back(i);
	return result;
}

std::vector<int> automaton_sizes_2(void)
{
	std::vector<int> result;
	for(int i=2; i<=20; i+=2) result.push_back(i);
	return result;
}

std::vector<int> tested_workgroup_sizes(void)
{
	std::vector<int> result;
	for(int i=1; i<=512; i*=2) result.push_back(i);
	return result;
}

void compare_word_store_patterns(void)
{
	auto context=get_first_GPU_context();
	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv_cuda; // vector of parameters
	std::vector<test_params> pv_cl;
	for(const auto& auto_size: automaton_sizes)
	{
		pv_cuda.push_back(default_test_params(auto_size, NULL, CUDA_DEFAULT_THREADS_PER_BLOCK));
		pv_cl.push_back(default_test_params(auto_size, &context));
	}

	auto cuda_concat_32 = avg_speed_v(pv_cuda, avg_speed_cuda_compressed_words_concatenated_int);
	auto cuda_interleaved_32 = avg_speed_v(pv_cuda, avg_speed_cuda_words_interleaved_uint32);
	auto cuda_interleaved_pattern=avg_speed_v(pv_cuda, avg_speed_cuda_interleaved_pattern);

	auto cl_concat_32 = avg_speed_v(pv_cl, avg_speed_cl_words_concatenated_compressed_int);
	auto cl_interleaved_32 = avg_speed_v(pv_cl, avg_speed_cl_interleaved_uint32);
	auto cl_interleaved_pattern=avg_speed_v(pv_cl, avg_speed_cl_interleaved_pattern);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));
	
	results.push_back(cuda_concat_32);
	results.push_back(cuda_interleaved_32);
	results.push_back(cuda_interleaved_pattern);
	results.push_back(cl_concat_32);
	results.push_back(cl_interleaved_32);
	results.push_back(cl_interleaved_pattern);

	output_vector_to_file(python_path+"cmp_store_patterns.txt", results);
}

void compare_square_nonsquare(void)
{
	auto context=get_first_GPU_context();
	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv_cuda; // vector of parameters
	std::vector<test_params> pv_cl;
	for(const auto& auto_size: automaton_sizes)
	{
		pv_cuda.push_back(default_test_params(auto_size, NULL, CUDA_DEFAULT_THREADS_PER_BLOCK));
		pv_cl.push_back(default_test_params(auto_size, &context));
	}

	auto cuda_nonsquare=avg_speed_v(pv_cuda, avg_speed_cuda_words_interleaved_uint32);
	auto cuda_square=avg_speed_v(pv_cuda, avg_speed_cuda_square_automaton);

	auto cl_nonsquare=avg_speed_v(pv_cl, avg_speed_cl_interleaved_uint32);
	auto cl_square=avg_speed_v(pv_cl, avg_speed_cl_square_automaton);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));

	results.push_back(cuda_nonsquare);
	results.push_back(cuda_square);
	results.push_back(cl_nonsquare);
	results.push_back(cl_square);

	output_vector_to_file(python_path+"cmp_square_nonsquare.txt", results);
}

void compare_compact_uncompact(void)
{
	//auto context=get_first_GPU_context();
	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv; // vector of parameters
	for(const auto& auto_size: automaton_sizes)
	{
		pv.push_back(default_test_params(auto_size, NULL));
	}

	auto uncompact_cuda=avg_speed_v(pv, avg_speed_cuda_words_interleaved_uint32);
	auto compact_cuda=avg_speed_v(pv, avg_speed_cuda_compact_words_interleaved_uint32);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));
	
	results.push_back(uncompact_cuda);
	results.push_back(compact_cuda);

	output_vector_to_file(python_path+"cmp_compact.txt", results);
}

void compare_texture_global_cl()
{
	auto context=get_first_GPU_context();
	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv; // vector of parameters
	for(const auto& auto_size: automaton_sizes)
	{
		pv.push_back(default_test_params(auto_size, &context));
	}

	auto global_cl=avg_speed_v(pv, avg_speed_cl_interleaved_uint32);
	auto texture_cl=avg_speed_v(pv, avg_speed_cl_texture);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));
	
	results.push_back(global_cl);
	results.push_back(texture_cl);

	output_vector_to_file(python_path+"cmp_glob_texture.txt", results);
}

void compare_pinned_unpinned_cl()
{
	auto context=get_first_GPU_context();
	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv; // vector of parameters
	for(const auto& auto_size: automaton_sizes)
	{
		pv.push_back(default_test_params(auto_size, &context));
	}

	auto unpinned_cl=avg_speed_v(pv, avg_speed_cl_interleaved_uint32);
	auto pinned_cl=avg_speed_v(pv, avg_speed_cl_pinned_interleaved_uint32);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));
	
	results.push_back(unpinned_cl);
	results.push_back(pinned_cl);

	output_vector_to_file(python_path+"cmp_pinned.txt", results);
}

void compare_cpu_gpu(void)
{
	auto context_gpu=get_first_GPU_context();
	auto context_cpu=get_first_CPU_context();

	auto automaton_sizes=automaton_sizes_1();

	std::vector<test_params> pv_gpu_cuda;
	std::vector<test_params> pv_gpu_cl;
	std::vector<test_params> pv_cpu;

	for(const auto& auto_size: automaton_sizes)
	{
		pv_gpu_cuda.push_back(default_test_params(auto_size, &context_gpu, CUDA_DEFAULT_THREADS_PER_BLOCK));
		pv_gpu_cl.push_back(default_test_params(auto_size, &context_gpu));
		pv_cpu.push_back(default_test_params(auto_size, &context_cpu, CPU_DEFAULT_THREADS_PER_BLOCK));
	}
	
	auto native_one_thread=avg_speed_v(pv_cpu, avg_speed_run_automaton_cpu);
	auto cuda_best=avg_speed_v(pv_gpu_cuda, avg_speed_cuda_words_interleaved_uint32);
	auto cl_best_gpu=avg_speed_v(pv_gpu_cl, avg_speed_cl_interleaved_uint32);
	auto cl_best_cpu=avg_speed_v(pv_cpu, avg_speed_cl_words_concatenated_compressed_int);

	auto cuda_dummy=avg_speed_v(pv_gpu_cuda, avg_speed_cuda_only_automaton_access_dummy);
	auto cl_dummy_gpu=avg_speed_v(pv_gpu_cl, avg_speed_cl_only_automaton_access_dummy);
	auto cl_dummy_cpu=avg_speed_v(pv_cpu, avg_speed_cl_only_automaton_access_dummy);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(automaton_sizes));
	
	results.push_back(native_one_thread);
	results.push_back(cuda_best);
	results.push_back(cl_best_gpu);
	results.push_back(cl_best_cpu);

	results.push_back(cuda_dummy);
	results.push_back(cl_dummy_gpu);
	results.push_back(cl_dummy_cpu);

	output_vector_to_file(python_path+"cmp_all_cpu_gpu.txt", results);
}

void compare_workgroup_sizes()
{
	auto context_gpu=get_first_GPU_context();
	auto context_cpu=get_first_CPU_context();

	auto work_sizes=tested_workgroup_sizes();
	std::vector<test_params> pv_gpu;
	std::vector<test_params> pv_cpu;

	const int STATE_BITS=20;

	for(const auto& work_size: work_sizes)
	{
		pv_gpu.push_back(test_params(
			STATE_BITS, 
			INPUT_OUTPUT_BITS, 
			INPUT_OUTPUT_BITS,
			DEFAULT_TEST_WORD_COUNT,
			DEFAULT_TEST_WORD_LENGTH,
			work_size,
			&context_gpu,
			NULL
		));

		pv_cpu.push_back(test_params(
			STATE_BITS, 
			INPUT_OUTPUT_BITS, 
			INPUT_OUTPUT_BITS,
			DEFAULT_TEST_WORD_COUNT,
			DEFAULT_TEST_WORD_LENGTH,
			work_size,
			&context_cpu,
			NULL
		));
	}

	auto cuda_interleaved=avg_speed_v(pv_gpu, avg_speed_cuda_words_interleaved_uint32);
	auto cl_gpu=avg_speed_v(pv_gpu, avg_speed_cl_interleaved_uint32);
	auto cl_cpu_concat=avg_speed_v(pv_cpu, avg_speed_cl_words_concatenated_compressed_int);

	vector<vector<double> > results;
	results.push_back(convert_to_double_vector(work_sizes));

	results.push_back(cuda_interleaved);
	results.push_back(cl_gpu);
	results.push_back(cl_cpu_concat);
	//results.push_back(cl_cpu_interleaved);

	output_vector_to_file(python_path+"cmp_workgroup_size.txt", results);
}

vector<int> tested_word_counts()
{
	vector<int> result;
	for(int i=128; i<=8192; i*=2) result.push_back(i);
	return result;
}

void compare_cpu_gpu_different_complects()
{
	auto context_gpu=get_first_GPU_context();
	auto context_cpu=get_first_CPU_context();

	auto word_counts=tested_word_counts();
	
	std::vector<test_params> pv_gpu_cuda;
	std::vector<test_params> pv_gpu_cl;
	std::vector<test_params> pv_cpu;

	const int STATE_BITS=20;
	//const int WORD_LENGTH=512;
	const int WORD_LENGTH=4096;

	for(const auto& word_count: word_counts)
	{
		pv_gpu_cuda.push_back(test_params(
			STATE_BITS, 
			INPUT_OUTPUT_BITS, 
			INPUT_OUTPUT_BITS,
			word_count,
			WORD_LENGTH,
			CUDA_DEFAULT_THREADS_PER_BLOCK,
			&context_gpu,
			NULL
		));

		pv_gpu_cl.push_back(test_params(
			STATE_BITS, 
			INPUT_OUTPUT_BITS, 
			INPUT_OUTPUT_BITS,
			word_count,
			WORD_LENGTH,
			OPENCL_DEFAULT_THREADS_PER_BLOCK,
			&context_gpu,
			NULL
		));

		pv_cpu.push_back(test_params(
			STATE_BITS, 
			INPUT_OUTPUT_BITS, 
			INPUT_OUTPUT_BITS,
			word_count,
			WORD_LENGTH,
			CPU_DEFAULT_THREADS_PER_BLOCK,
			&context_cpu,
			NULL
		));
	}

	auto native_one_thread=avg_speed_v(pv_cpu, avg_speed_run_automaton_cpu);
	auto cuda_best=avg_speed_v(pv_gpu_cuda, avg_speed_cuda_words_interleaved_uint32);
	auto cl_best_gpu=avg_speed_v(pv_gpu_cl, avg_speed_cl_interleaved_uint32);
	auto cl_best_cpu=avg_speed_v(pv_cpu, avg_speed_cl_words_concatenated_compressed_int);

	vector<vector<double> > results;
	//results.push_back(convert_to_double_vector(work_sizes));
	results.push_back(convert_to_double_vector(word_counts));
	vector<double> sizes;
	sizes.push_back(STATE_BITS);
	sizes.push_back(WORD_LENGTH);
	
	results.push_back(sizes);

	results.push_back(native_one_thread);
	results.push_back(cuda_best);
	results.push_back(cl_best_gpu);
	results.push_back(cl_best_cpu);
	//results.push_back(cl_cpu_interleaved);

	output_vector_to_file(python_path+"cmp_different_complects.txt", results);
}