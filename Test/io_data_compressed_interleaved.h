/**
	\file io_data_compressed_interleaved.h
	\brief declaration of io_dat_c_par class
*/

#pragma once
#include "io_data_compressed.h" // io_data_compressed class

// TODO - better example
/**
	\brief stores words in parallel(interleaved way)

	Example:

*/
template<typename T>
class io_data_compressed_interleaved
	: public io_data_compressed
{
public:
	T* input_compressed_interleaved; ///< interleaved compressed input word complect
	T* output_compressed_interleaved; ///< interleaved compressed output word complect
	const int T_length; // TODO - comment

	io_data_compressed_interleaved(const word_params& p): ///< see parent
		io_data_compressed(p),
		T_length(compressed_word_length/(sizeof(T)/sizeof(**input_compressed)))
	{
		input_compressed_interleaved=new T[T_length*p.word_count];
		output_compressed_interleaved=new T[T_length*p.word_count];
	}

	~io_data_compressed_interleaved() ///< see parent
	{
		delete[] input_compressed_interleaved;
		delete[] output_compressed_interleaved;
	}

	virtual void refill_words(void) ///< see parent
	{
		io_data_compressed::refill_words();

		for(int i=0; i<p.word_count; i++)
		{
			T* current_word=(T*)(input_compressed[i]);

			for(int j=0; j<T_length; j++)
			{
				input_compressed_interleaved[j*p.word_count+i]=current_word[j];
			}
		}
	}

	virtual void prepare_output(void) ///< see parent
	{
		for(int i=0; i<p.word_count; i++)
		{
			T* current_word=(T*)(output_compressed[i]);

			for(int j=0; j<T_length; j++)
			{
				current_word[j]=output_compressed_interleaved[j*p.word_count+i];
			}
		}

		io_data_compressed::prepare_output();
	}
private:
	io_data_compressed_interleaved(const io_data_compressed_interleaved& other); ///< prohibit copy
	io_data_compressed_interleaved& operator=(const io_data_compressed_interleaved& other);
protected:

};