/**
	\file automaton_data.h
	\brief declaration of automaton_data class
*/

#pragma once
#include "automaton_host.h"		// automaton_host class
#include "test_params.h"		// TODO

using namespace common_auto;

/**
	\brief Wrapper class for automaton

	This and inherited classes are used to hide differences
	between automaton, automaton_CUDA and automaton_CL in tests
*/
class automaton_data
{
public:
	automaton_host host_automaton; ///< automaton representation in host memory
	
	/**
		\brief kind of awkward tesing mechanism:
		if you want to test using square automaton, you specify it in initializing parameters
	*/
	automaton_host* square_host_automaton;
	const automaton_params p; ///< parameters for automaton

	/**
		\brief constructor from parameters
		\param params size of automaton to be created
	*/
	automaton_data(const automaton_params& params);

	/**
		\brief empty destructor
	*/
	~automaton_data();

	/**
		\brief refills host_automaton table with random values
	*/
	virtual void refill_automaton(void);
private:
	automaton_data(const automaton_data& other); ///< prohibit copy
	automaton_data& operator=(const automaton_data& other);
protected:
	
};
