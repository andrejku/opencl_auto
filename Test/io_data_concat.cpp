#pragma once

#include "io_data_concat.h"
#include <cstring>

io_data_concat::io_data_concat(const word_params &p):
	io_data(p)
{
	input_concat=new uint8_t[p.word_count*word_length_padded/*p.word_length*/];
	output_concat=new uint8_t[p.word_count*word_length_padded/*p.word_length*/];
}

io_data_concat::~io_data_concat()
{
	delete[] input_concat;
	delete[] output_concat;
}

void io_data_concat::refill_words(void)
{
	io_data::refill_words();
	for(int i=0; i<p.word_count; i++)
	{
		memcpy(
			input_concat+i*word_length_padded/*p.word_length*/, 
			input[i], 
			word_length_padded/*p.word_length*/*sizeof(*input_concat)
		);
	}
}

void io_data_concat::prepare_output(void)
{
	for(int i=0; i<p.word_count; i++)
	{
		memcpy(
			output[i], 
			output_concat+i*word_length_padded/*p.word_length*/,
			word_length_padded/*p.word_length*/*sizeof(*output_concat)
		);
	}

	io_data::prepare_output();
}