#include "test_utility.h"		// declaration of functions
#include "random_functions.h"	// ceiling

static const int SHIFT_BITS=4;
static const unsigned char SHIFT_MASK=(1<<SHIFT_BITS)-1;

namespace common_auto
{

unsigned char* pack_two_uint8(
	const uint8_t* word, 
	uint8_t* result, 
	int length
)
{
	int new_length=ceiling(length,2);
	int full_iterations=length/2;

	for(int i=0; i<full_iterations; i++)
	{
		result[i]=(word[2*i+1]<<SHIFT_BITS)|word[2*i];
	}

	if(length%2!=0)
	{
		result[new_length-1]=word[length-1]&SHIFT_BITS;
	}

	return result;
}

void unpack_two_uint8(
	const uint8_t* word, 
	uint8_t* result, 
	int new_length
)
{
	int full_iterations=new_length/2;

	for(int i=0; i<full_iterations; i++)
	{
		result[2*i]=word[i]&SHIFT_MASK;
		result[2*i+1]=word[i]>>SHIFT_BITS;
	}

	if(new_length%2!=0)
	{
		result[new_length-1]=word[new_length/2]&SHIFT_MASK;
	}
}

}