
#pragma once
#include "cl_pinned_words.h"
#include "io_data_compressed_interleaved.h"

using namespace cl_auto;

// TODO: move implementation to .cpp

class io_data_pinned_interleaved_cl
	: public io_data_compressed_interleaved<uint32_t>
{
public:
	cl_pinned_words pinned_words;

	io_data_pinned_interleaved_cl(const word_params& p):
		io_data_compressed_interleaved<uint32_t>(p),
		pinned_words(
			T_length*p.word_count*sizeof(uint32_t),
			*p.cl_context
		)
	{

	}

	~io_data_pinned_interleaved_cl()
	{

	}

	void refill_words(void)
	{
		io_data_compressed_interleaved<uint32_t>::refill_words();

		void *mapped_input=pinned_words.map_input();
		//std::cout << "Address of mapped input: " << int(mapped_input) << std::endl;

		memcpy(mapped_input, input_compressed_interleaved, T_length*p.word_count*sizeof(uint32_t));
	}

	void prepare_output(void)
	{
		void *mapped_output=pinned_words.map_output();
		//std::cout << "Address of mapped output: " << int(mapped_output) << std::endl;

		void *mapped_input=pinned_words.map_input();

		memcpy(output_compressed_interleaved, mapped_output, T_length*p.word_count*sizeof(uint32_t));
		io_data_compressed_interleaved<uint32_t>::prepare_output();
	}
private:
	io_data_pinned_interleaved_cl(const io_data_pinned_interleaved_cl& other); ///< prohibit copy
	io_data_pinned_interleaved_cl& operator=(const io_data_pinned_interleaved_cl& other);
protected:
};