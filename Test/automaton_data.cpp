#pragma once

#include "automaton_data.h"

automaton_data::automaton_data(const automaton_params& params):
	host_automaton(params.state_bits, params.input_bits, params.output_bits),
	p(params),
	square_host_automaton(NULL)
{
	if(p.square_automaton)
	{
		square_host_automaton=new automaton_host(p.state_bits, 2*p.input_bits, 2*p.output_bits);
		//square_host_automaton.square_automaton(host_automaton); // table wasn't filled (filled with crap), squaring won't be correct
	}
}

automaton_data::~automaton_data()
{
	if(p.square_automaton)
	{
		delete square_host_automaton;
	}
}

void automaton_data::refill_automaton(void)
{
	host_automaton.rand_fill(false);
	if(p.square_automaton)
	{
		automaton_host::square_automaton(host_automaton, *square_host_automaton);
	}
}