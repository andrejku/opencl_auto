/**
	\file io_data_compressed.h
	\brief declaration of io_data_compressed class
*/

#pragma once
#include "io_data.h" // io_data class

/**
	\brief stores words in compressed way

	Input and output for automaton are stored in compressed way
	This means that 2*i-th and 2*i+1-th symbol of word are represented
	as higher 4 and lower 4 bits of of i-th input/output character
*/
class io_data_compressed
	: public io_data
{
public:
	uint8_t** input_compressed; ///< compressed input word complect
	uint8_t** output_compressed; ///< compressed output word complect
	const int compressed_word_length; // TODO --comment
	io_data_compressed(const word_params& params); ///< see parent
	~io_data_compressed(); ///< see parent
	virtual void refill_words(void); ///< see parent
	virtual void prepare_output(void); /// < see parent
private:
	io_data_compressed(const io_data_compressed& other); ///< prohibit copy
	io_data_compressed& operator=(const io_data_compressed& other);
protected:

};