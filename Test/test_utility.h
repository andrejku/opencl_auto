/**
	\file test_utility.h
	\brief some functions used in handling word complects
*/

#pragma once
#include <cassert>	// assert
#include <new>		// std::nothrow
#include <cstdint>  // intx_t

namespace common_auto
{

/** 
	\brief allocates array of arrays (word complect)
	\tparam T type to be allocated
	\param word_count number of rows (words)
	\param word_length number of columns (length in characters)
	\return ponter to 2D-array
*/
template<typename T>
T** alloc_words(int word_count, int word_length)
{
	T ** result=new (std::nothrow) T* [word_count];
	assert(result!=NULL);

	for(int i=0; i<word_count; i++)
	{
		result[i]=new (std::nothrow) T[word_length];
		assert(result!=NULL);
	}

	return result;
}

/**
	\brief deletes array of arrays (word complect)
	\tparam T type to be deleted
	\param words pointer to 2D-array to be deleted
	\param word_count number of rows in array
*/
template<typename T>
void delete_words(T** words, int word_count)
{
	for(int i=0; i<word_count; i++)
	{
		delete[] words[i];
	}

	delete[] words;
}

/**
	\brief compresses two unsigned chars to one
	
	word[2*i] and word[2*i+1] will be stored as 
	lower 4 and higher 4 bits of result[i]
*/
unsigned char* pack_two_uint8(
	const uint8_t* word, 
	uint8_t* result, 
	int length
);

/**
	\brief decompresses one unsigned char to two
	
	result[2*i] and result[2*i+1] will be equal to 
	higher 4 and lower 4 bits of word[i]
*/
void unpack_two_uint8(
	const uint8_t* word, 
	uint8_t* result, 
	int new_length
);

}