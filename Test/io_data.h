/**
	\file io_data.h
	\brief declaration of io_data class
*/

#pragma once
#include "test_params.h"		// TODO
#include "random_functions.h"	// rand_char
#include <cstdint>				// intx_t

using namespace common_auto;

// TODO: better - comment?; replace 2 by symbolic constant
// this is a sort of hack
// io_data pads each word before transformations take place
// (so that word_length would be divisible by PAD_SIZE)
const int PAD_SIZE=2*sizeof(uint32_t)/sizeof(uint8_t);

/**
	\brief stores words in uncompressed way
	\brief responsibility of this and inherited classes is managing input/output words

	This and inherited classes allocate, free, check for correctness,
	prepare and refill input and output words.

	The class is used to hide differences between 
	different ways of storing words in memory
*/
class io_data
{
public:
	uint8_t** input; 				///< input word complect in host memory
	uint8_t** output;				///< output from tested algorithm
	uint8_t** output_regular;		///< output from correct algorithm
	const word_params p;			///< parameters for word complect
	const int word_length_padded;	// TODO - comment

	/**
		\brief constructor from parameters
		\param params size of word complect
	*/
	io_data(const word_params& params);
	
	/**
		\brief will free allocated input and output words
	*/
	~io_data();

	/**
		\brief refills input word complect with random values
	*/
	virtual void refill_words(void);

	/**
		\brief prepares output words for checking
	*/
	virtual void prepare_output();
	
	/**
		\brief checks if words in output and output_regular are the same
		
		Returns true, if words are the same
	*/
	bool check_correct(void);

	/**
		\brief calculates number of processed symbols
	*/
	int symbols_processed(void);
private:
	io_data(const io_data& other); ///< prohibit copy
	io_data& operator=(const io_data& other);
protected:

};