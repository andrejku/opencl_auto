#pragma once
#include "automaton_data.h"
#include "automaton_compact_cuda.h"
#include <cassert>
#include "cuda_helper.h"

using namespace cuda_auto;

// TODO - hide to .cpp
class cuda_compact_automaton_data
	: public automaton_data
{
public:
	automaton_compact_CUDA cuda_automaton;
	
	cuda_compact_automaton_data(const automaton_params& params)
		: automaton_data(params),
		cuda_automaton(host_automaton)
	{
		//cuda_automaton=*create_cuda_compact_automaton(host_automaton);
	}

	~cuda_compact_automaton_data()
	{

	}

	// TODO - this is ugly (allocating memory each time to compress to 16 bits)
	void refill_automaton(void)
	{
		automaton_data::refill_automaton();
		uint16_t* temp_table=new (std::nothrow) uint16_t[host_automaton.table_size];
		assert(temp_table!=NULL);

		for(int i=0; i<host_automaton.table_size; i++)
		{
			temp_table[i]=uint16_t(host_automaton.table[i]);
		}

		cudaError table_copy_error=cudaMemcpy(
			(void*) cuda_automaton.table,
			(const void*) temp_table,
			host_automaton.table_size*sizeof(*cuda_automaton.table),
			cudaMemcpyKind::cudaMemcpyHostToDevice
		);
		cuda_assert(table_copy_error);

		delete[] temp_table;
	}
private:
	cuda_compact_automaton_data(const cuda_compact_automaton_data& other);
	cuda_compact_automaton_data& operator=(const cuda_compact_automaton_data& other);
protected:
	
};