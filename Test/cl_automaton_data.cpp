#pragma once

#include "cl_automaton_data.h"
#include "CL_helper.h"

cl_automaton_data::cl_automaton_data(
	const automaton_params& params
):
	automaton_data(params),
	cl_automaton(params.square_automaton ? *square_host_automaton : host_automaton, *params.cl_context)
{

}

cl_automaton_data::~cl_automaton_data()
{

}

void cl_automaton_data::refill_automaton(void)
{
	automaton_data::refill_automaton();
	cl::CommandQueue queue(*cl_automaton.context);

	automaton_host* automaton_to_copy;
	if(p.square_automaton) 
	{
		automaton_to_copy=square_host_automaton;
	}
	else 
	{
		automaton_to_copy=&host_automaton;
	}

	cl_int table_copy_error = queue.enqueueWriteBuffer( // TODO - this is bad, because code got copypasted
		cl_automaton.table,
		CL_TRUE, 
		0,
		automaton_to_copy->table_size*sizeof(*host_automaton.table),
		(void*) automaton_to_copy->table
	);
	cl_assert(table_copy_error);
}
