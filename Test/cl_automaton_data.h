/**
	\file cl_automaton_data.h
	\brief declaration of cl_automaton_data class
*/

#pragma once
#include "automaton_CL.h"	// automaton_CL structure
#include "automaton_data.h"	// automaton_data class

using namespace cl_auto;

/**
	\brief wrapper class for automaton_CL struct
*/
class cl_automaton_data
	: public automaton_data
{
public:
	automaton_CL cl_automaton; //< automaton representation on OpenCL capable device 
	cl_automaton_data(const automaton_params& params); ///< see parent
	~cl_automaton_data(); ///< see parent
	void refill_automaton(void); ///< see parent
private:
	cl_automaton_data(const cl_automaton_data& other); ///< prohibit copy
	cl_automaton_data& operator=(const cl_automaton_data& other);
protected:

};