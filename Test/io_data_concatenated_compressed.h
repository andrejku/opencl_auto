/**
	\file io_data_concatenated_compressed.h
	\brief declaration of io_data_concatenated_compressed class
*/

#pragma once
#include "io_data_compressed.h" // io_data_compressed class

// TODO -better example
/**
	\brief stores words in compressed way, one after the other

	Input/output words are stored in contiguous memory block one after the other
*/
class io_data_concatenated_compressed
	: public io_data_compressed
{
public:
	uint8_t* input_concatenated_compressed; ///< compressed concatenated input word complect
	uint8_t* output_concatenated_compressed; ///< compressed concatenated output word complect
	io_data_concatenated_compressed(const word_params& params); ///< see parent
	~io_data_concatenated_compressed(); ///< see parent
	virtual void refill_words(void); ///< see parent
	virtual void prepare_output(void); ///< see parent
private:
	io_data_concatenated_compressed(const io_data_concatenated_compressed& other); ///< prohibit copy
	io_data_concatenated_compressed& operator=(const io_data_concatenated_compressed& other);
protected:
	
};