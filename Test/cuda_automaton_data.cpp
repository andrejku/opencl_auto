#pragma once

#include "cuda_automaton_data.h"
#include <cuda_runtime.h>
#include "cuda_helper.h"

cuda_automaton_data::cuda_automaton_data(const automaton_params& params):
	automaton_data(params),
	//cuda_automaton(host_automaton)
	cuda_automaton(params.square_automaton ? *square_host_automaton : host_automaton)
{

}

cuda_automaton_data::~cuda_automaton_data()
{

}

void cuda_automaton_data::refill_automaton(void)
{
	automaton_data::refill_automaton();

	automaton_host* automaton_to_copy;
	if(p.square_automaton) 
	{
		automaton_to_copy=square_host_automaton;
	}
	else 
	{
		automaton_to_copy=&host_automaton;
	}

	cudaError table_copy_error = cudaMemcpy( // TODO - this is bad, because code got copypasted
		(void*) cuda_automaton.table,
		(void*) automaton_to_copy->table,
		automaton_to_copy->table_size*sizeof(*cuda_automaton.table),
		cudaMemcpyKind::cudaMemcpyHostToDevice
	);
	cuda_assert(table_copy_error);
}