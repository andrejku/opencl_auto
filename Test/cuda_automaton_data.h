/**
	\file cuda_automaton_data.h
	\brief declaration of cuda_automaton_data class
*/

#pragma once
#include "automaton_data.h"		// automaton_data class
#include "automaton_cuda.h"

using namespace cuda_auto;

/**
	\brief wrapper class for automaon_CUDA struct
*/
class cuda_automaton_data
	: public automaton_data
{
public:
	automaton_CUDA cuda_automaton; ///< representation on CUDA-capable device
	cuda_automaton_data(const automaton_params& params); ///< see parent
	~cuda_automaton_data(); ///< see parent
	void refill_automaton(void); ///< see parent
private:
	cuda_automaton_data(const cuda_automaton_data& other); ///< prohibit copy
	cuda_automaton_data& operator=(const cuda_automaton_data& other);
protected:

};