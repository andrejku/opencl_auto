/**
	\file test_correct.j
	\brief functions that test correctness of automaton simulation
*/

#pragma once

#include "launch_definitions.h"
#include "CL_helper.h"

/**
	\brief generic function to test corectness of automaton simulation
	\tparam T_auto type of automaton_data
	\tparam T_words type of io_data
	\param v_p vector that holds parameters for different test cases
	\param call_automaton function that simulates automaton
*/
template<typename T_auto, typename T_words>
bool test_correct_generic(
	const std::vector<test_params> &v_p,
	void (*call_automaton) (const T_auto&, T_words&, const test_params&)
)
{
	const static int WORD_COMPLECTS_PER_AUTOMATON=2;
	const static int DIFFERENT_AUTOMATONS=5;

	bool result=true;

	for(const auto& p: v_p)
	{
		T_words words_data(p.p_words);
		T_auto automaton_data(p.p_auto);

		for(int au=0; au<DIFFERENT_AUTOMATONS; au++)
		{
			automaton_data.refill_automaton();
			for(int w_compl=0; w_compl<WORD_COMPLECTS_PER_AUTOMATON; w_compl++)
			{
				words_data.refill_words();

				launch_automaton_native_correct(automaton_data, words_data, p);
				call_automaton(automaton_data, words_data, p);

				if(!words_data.check_correct()) result=false;
			}
		}
	}

	return result;
}

std::vector<test_params> default_test_correct_params(
	cl::Context* context=NULL,
	cl::Kernel* kernel=NULL,
	int state_bits=20
)
{
	//int TC_STATE_BITS = 20; // TC for Test Correct
	int TC_STATE_BITS=state_bits;
	int TC_IO_BITS = 4;
	int TC_WORD_COUNT = 100;
	int TC_WORD_LENGTH =1000;
	int TC_THREAD_COUNT=64;

	std::vector<test_params> v_p;
	for(int word_length=TC_WORD_LENGTH; word_length<TC_WORD_LENGTH+PAD_SIZE; word_length++)
	{
		v_p.push_back(test_params(
			TC_STATE_BITS,
			TC_IO_BITS,
			TC_IO_BITS,
			TC_WORD_COUNT,
			word_length,
			TC_THREAD_COUNT,
			context,
			kernel
		));
	}

	return v_p;
}

bool test_correct_cuda_compressed_words_concatenated_int(void)
{
	auto v_p=default_test_correct_params();

	return test_correct_generic
		<cuda_automaton_data,io_data_concatenated_compressed>
			(v_p, launch_cuda_compressed_words_concatenated_int);
}

bool test_correct_cuda_words_interleaved_uint32(void)
{
	auto v_p=default_test_correct_params();

	return test_correct_generic
		<cuda_automaton_data,io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cuda_words_interleaved_uint32);
}

bool test_correct_cuda_interleaved_pattern(void)
{
	auto v_p=default_test_correct_params();

	return test_correct_generic
		<cuda_automaton_data,io_data_concatenated_compressed>
			(v_p, launch_cuda_interleaved_pattern);
}

bool test_correct_cuda_square_automaton(void)
{
	auto v_p=default_test_correct_params(NULL, NULL, 16);
	for(auto& p: v_p)
	{
		p.p_auto.square_automaton=true;
	}

	return test_correct_generic
		<cuda_automaton_data,io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cuda_square_automaton);
}

bool test_correct_cuda_compact_words_interleaved_uint32(void)
{
	auto v_p=default_test_correct_params(NULL, NULL, 12);
	
	return test_correct_generic
		<cuda_compact_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cuda_compact_words_interleaved_uint32);
}

bool test_correct_cl_words_concatenated_compressed_int(void)
{
	cl::Context context=get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "concat_compressed_int.cl", "run_automaton_compressed_int", "");

	auto v_p=default_test_correct_params(&context, &kernel);

	return test_correct_generic
		<cl_automaton_data, io_data_concatenated_compressed>
			(v_p, launch_cl_words_concatenated_compressed_int);
}

bool test_correct_cl_interleaved_uint32(void)
{
	cl::Context context=get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");

	auto v_p=default_test_correct_params(&context, &kernel);

	return test_correct_generic
		<cl_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cl_interleaved_uint32);
}

bool test_correct_cl_interleaved_pattern(void)
{
	cl::Context context=get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "interleaved_pattern.cl", "run_automaton_interleaved_pattern", "");

	auto v_p=default_test_correct_params(&context, &kernel);

	return test_correct_generic
		<cl_automaton_data, io_data_concatenated_compressed>
			(v_p, launch_cl_interleaved_pattern_uint32);
}

bool test_correct_cl_interleaved_uint32_cpu(void)
{
	cl::Context context=get_first_CPU_context();//get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");

	auto v_p=default_test_correct_params(&context, &kernel);

	return test_correct_generic
		<cl_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cl_interleaved_uint32);
}

bool test_correct_cl_square_automaton(void)
{
	cl::Context context=get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "square_automaton.cl", "run_square_automaton_uint32", "");

	auto v_p=default_test_correct_params(&context, &kernel, 16);

	for(auto& p: v_p)
	{
		p.p_auto.square_automaton=true;
	}

	return test_correct_generic
		<cl_automaton_data, io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cl_square_automaton);
}

bool test_correct_cl_pinned_interleaved(void)
{
	cl::Context context = get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "interleaved_uint32.cl", "run_automaton_symbols_interleaved_uint32", "");

	auto v_p=default_test_correct_params(&context, &kernel);

	
	return test_correct_generic
		<cl_automaton_data, io_data_pinned_interleaved_cl>
			(v_p, launch_cl_pinned_interleaved);
}

bool test_correct_cl_texture_automaton(void)
{
	cl::Context context=get_first_GPU_context();
	cl::Kernel kernel=create_kernel(context, "texture_interleaved_uint32.cl", "run_automaton_texture_words_interleaved_uint32", "");

	auto v_p=default_test_correct_params(&context, &kernel, 10);

	return test_correct_generic
		<cl_automaton_texture_data, io_data_compressed_interleaved<uint32_t>>
			(v_p, launch_cl_texture);
}

void all_test_correct(void)
{
	test_correct_cuda_compressed_words_concatenated_int();

	test_correct_cuda_words_interleaved_uint32();

	test_correct_cuda_interleaved_pattern();

	test_correct_cuda_square_automaton();
	
	test_correct_cuda_compact_words_interleaved_uint32();

	test_correct_cl_words_concatenated_compressed_int();

	test_correct_cl_interleaved_uint32();

	test_correct_cl_square_automaton();

	test_correct_cl_interleaved_pattern();

	test_correct_cl_texture_automaton();
	test_correct_cl_pinned_interleaved();
}