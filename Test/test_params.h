/**
	\file test_params.h
	\brief structs that hold test parameters
*/

#pragma once

#include <CL/cl.hpp> // cl::Context

/**
	\brief contains sizes of automaton and context to which automaton should belong
*/
struct automaton_params
{
	int state_bits;	///< number of bits for automaton_state
	int input_bits;	///< number of bits for input symbol
	int output_bits;	///< number of bits for output symbol
	cl::Context* cl_context; ///< context, to which automaton is associated
	bool square_automaton; ///< if true, then simulate, using squared automaton (when applicable)

	automaton_params(
		int _state_bits, 
		int _input_bits, 
		int _output_bits,
		cl::Context* target_cl_context,
		bool _square_automaton=false
	):
		state_bits(_state_bits),
		input_bits(_input_bits),
		output_bits(_output_bits),
		cl_context(target_cl_context),
		square_automaton(_square_automaton)
	{

	}

	~automaton_params()
	{
		// yup, empty destructor
	}
};

/**
	\brief contains sizes of word complect
*/
struct word_params
{
	int word_count;	///< number of different words in complect
	int word_length;	///< length of word (in 4 bit symbols)
	int input_bits;	///< number of bits for input symbol
	cl::Context* cl_context;
	
	word_params(
		int _word_count, 
		int _word_length, 
		int _input_bits,
		cl::Context* target_cl_context
	):
		word_count(_word_count),
		word_length(_word_length),
		input_bits(_input_bits),
		cl_context(target_cl_context)
	{

	}

	~word_params()
	{
		// yup, empty destructor
	}
};

/**
	\brief struct that contains test parameters
*/
struct test_params
{
	automaton_params p_auto;	///< automaton parameters
	word_params p_words;		///< word paramseters
	int threads;	///< number of threads per workgroup (OpenCL) or per thread-block (CUDA)
	cl::Kernel *kernel;	///< kernel that will be used when automaton will be simulatad with OpenCL

	test_params(
		int state_bits, 
		int input_bits, 
		int output_bits,
		int word_count,
		int word_length,
		int threads_per_group,
		cl::Context* target_cl_context,
		cl::Kernel* _kernel,
		bool square_automaton=false
	):
		p_auto(state_bits, input_bits, output_bits, target_cl_context, square_automaton),
		p_words(word_count, word_length, input_bits, target_cl_context),
		threads(threads_per_group)
	{
		kernel=_kernel;
	}

	~test_params()
	{
		// yup, empty destructor
	}
};
