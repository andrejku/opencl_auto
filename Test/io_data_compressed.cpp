#pragma once
#include "io_data_compressed.h"
#include "random_functions.h"
#include "test_utility.h"

using namespace common_auto;

io_data_compressed::io_data_compressed(const word_params& params):
	io_data(params),
	compressed_word_length(ceiling(word_length_padded, 2))
{
	input_compressed=
		alloc_words<uint8_t>(p.word_count, compressed_word_length);
	output_compressed=
		alloc_words<uint8_t>(p.word_count, compressed_word_length);
}

io_data_compressed::~io_data_compressed()
{
	delete_words(input_compressed, p.word_count);
	delete_words(output_compressed, p.word_count);
}
	
void io_data_compressed::refill_words(void)
{
	io_data::refill_words();
	
	for(int i=0; i<p.word_count; i++)
	{
		pack_two_uint8(input[i], input_compressed[i], word_length_padded/*p.word_length*/);
	}
}


void io_data_compressed::prepare_output(void)
{
	for(int i=0; i<p.word_count; i++)
	{
		unpack_two_uint8(output_compressed[i], output[i], word_length_padded/*p.word_length*/);
	}

	io_data::prepare_output();
}