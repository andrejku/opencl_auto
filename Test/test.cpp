/**
	\file test.cpp
	\brief entry point of test application
*/

#define SILENCE 0
#define FAST_PERFORMANCE_TEST 2

#include "test_correct.h"	// correctness tests
#include "avg_speed.h"		// average speed performance tests
#include <iostream>			// std::cout, std::cin -- TODO?
#include "misc_tests.h"

using std::cout;
using std::endl;
using namespace cl_auto;

void another_assignment_test(void)
{
	automaton_host A(10,4,4);
	automaton_host B(10,8,8);
	automaton_host::square_automaton(A, B);
}

int main()
{
	//another_assignment_test();
	//dummy();
	//compare_word_store_patterns();
	
	//compare_texture_global_cl();
	//compare_compact_uncompact();
	//compare_cpu_gpu();
	//compare_workgroup_sizes();
	//compare_pinned_unpinned_cl();
	
	//compare_cpu_gpu_different_complects();

	//test_correct_cuda_compressed_words_concatenated_int();
	//test_correct_cl_interleaved_uint32_cpu();
	//test_correct_cl_texture_automaton();
	all_test_correct();

	//test_memory_cl_pinned_words();
	//test_memory_cl_pinned_words_2();
	//test_memory_cl_pinned_words_3();
	//test_memory_automaton_CL();
	//test_memory_automaton_CL_text();

	//test_correct_cuda_words_interleaved_uint32();
	//test_correct_cuda_square_automaton();
	//test_correct_cl_interleaved_uint32();
	//test_correct_cl_interleaved_pattern();

	//compare_square_nonsquare();
	//test_correct_cl_square_automaton();

	return 0;
}
