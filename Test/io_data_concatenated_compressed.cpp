#pragma once

#include "io_data_concatenated_compressed.h"

io_data_concatenated_compressed
	::io_data_concatenated_compressed(const word_params& params):
		io_data_compressed(params)
{
	input_concatenated_compressed=
		new uint8_t[compressed_word_length*p.word_count];

	output_concatenated_compressed=
		new uint8_t[compressed_word_length*p.word_count];
}

io_data_concatenated_compressed::~io_data_concatenated_compressed()
{
	delete[] input_concatenated_compressed;
	delete[] output_concatenated_compressed;
}
	
void io_data_concatenated_compressed::refill_words(void)
{
	io_data_compressed::refill_words();

	for(int i=0; i<p.word_count; i++)
	{
		memcpy(
			input_concatenated_compressed+i*compressed_word_length,
			input_compressed[i],
			compressed_word_length
		);
	}
}


void io_data_concatenated_compressed::prepare_output(void)
{
	for(int i=0; i<p.word_count; i++)
	{
		memcpy(
			output_compressed[i],
			output_concatenated_compressed+i*compressed_word_length,
			compressed_word_length
		);
	}

	io_data_compressed::prepare_output();
}