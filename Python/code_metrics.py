__author__ = 'ak'

import os

def line_count(file_path):
    f=open(file_path)
    lines=f.readlines()
    return len(lines)

line_widths ={ }

def width_spread(file_path):
    f=open(file_path)
    lines=f.readlines()

    for line in lines:
        l=len(line)-1
        if l in line_widths:
            line_widths[l] +=1
        else:
            line_widths[l] = 1


import re

def comment_lines(file_path):
    f=open(file_path)
    lines=f.readlines()

    result=0
    state=0

    for line in lines:
        if line.startswith("//"):
            result+=1
        else:
            if re.match('/\*', line):
                state=1
            result+=state
            if(re.match('\*/', line)):
                state=0

    return result

import numpy as np
import matplotlib.pyplot as plt


def calculate():
    #path = '..\\'
    #path='..\\Common\\'
    path='..\\OpenCL_Auto\\'
    #path='..\\CUDA_Auto\\'
    #path='..\\Test\\'
    #extensions = ['h', 'hpp', 'cpp', 'c', 'cu', 'cl', 'py']
    extensions = ['h', 'hpp', 'cpp', 'c', 'cu', 'cl']
    #extensions = ['h', 'hpp', 'cpp', 'c', 'cu']


    total_line_count = 0
    total_files = 0
    #total_not_small_lines=0
    total_comment_lines=0

    files_lines=[]

    for root, dirs, files in os.walk(path):
        for f in files:
            ok=False
            for ext in extensions:
                if f.endswith('.' + ext):
                    ok=True
            if ok:
                full_path=root + '\\' + f
                lc=line_count(full_path)
                total_line_count += lc
                files_lines.append((lc, f))

                total_files+= 1
                width_spread(full_path)
                total_comment_lines += comment_lines(full_path)

    files_lines.sort(reverse=True)
    for (lc, f, ) in files_lines:
        print('File: ' + f + ' , lines: ' + str(lc))

    print('Files: ' + str(total_files))
    print('Lines: ' + str(total_line_count))
    print('Lines that are (at least partially) commented out ' + str(total_comment_lines))

    lws=[]
    lcs=[]

    for (lw, lc) in line_widths.items():
        lws.append(lw)
        lcs.append(lc)

    plt.plot(lws, lcs)
    plt.show()


calculate()