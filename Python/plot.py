__author__ = 'Andrejs Kuzņecovs'
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import matplotlib as mpl

mpl.rcParams['lines.linewidth'] = 2
mpl.rcParams['savefig.dpi'] = 150
mpl.rcParams['figure.subplot.top'] = 0.85
matplotlib.rc('font', **{'sans-serif' : 'Arial', 'family' : 'sans-serif'})
increase_plot_height=1.1

def plt_common_part(wc, wl, img_name):
    plt.legend()

    #plt.xlabel("State bits of automaton")
    plt.xlabel('Automāta stāvokļu biti')

    #plt.ylabel("Execution speed (symbols/ms)")
    plt.ylabel("Izpildes ātrums (simboli/ms)")

    # plt.title(
    #     '\n'.join(
    #         ['Speed of transducer=f(automaton size)',
    #          str(int(wc)) + ' input words',
    #          'input length - ' + str(int(wl)) +
    #          ' symbols',
    #          'in, out symbols - 4 bits'
    #         ])
    # )

    plt.title(
        '\n'.join(
            ['Automāta ātrums = f(automāta izmērs)',
             str(int(wc)) + ' vārdi, vārdu garums - ' + str(int(wl)) +' simboli',
             'ieejas, izejas simboli - 4 biti'
            ])
    )

    lim_y=plt.ylim()
    plt.ylim(0, increase_plot_height*lim_y[1])
    plt.savefig(img_name+'.png')
    #plt.savefig(img_name+'.pdf')
    plt.show()
    #plt.clf()

def default_wc_wl():
    return [1024, 4096]

def div_lst(list_A, list_B):
    return [a/b for(a, b) in zip(list_A, list_B)]

# trim leading zeros
def trim_both(list_A, list_B):
    i=0
    while list_A[i]==0:
        i+=1
    return [list_A[i:], list_B[i:]]

def cmp_contiguous_uncontiguous():
    filename='cmp_contig_uncontig.txt'
    [x, y_cuda_separate, y_cuda_concat, y_cl_separate, y_cl_concat]=np.loadtxt(filename)

    #plt.plot(x, y_cuda_separate, label='CUDA, words stored separately')
    plt.plot(x, y_cuda_separate, label='CUDA, vārdi glabāti atsevišķi')

    #plt.plot(x, y_cuda_concat, label='CUDA, words concatenated')
    plt.plot(x, y_cuda_concat, label='CUDA, saķēdēti vārdi')

    #plt.plot(x, y_cl_separate, label='OpenCL, words stored separately')
    plt.plot(x, y_cl_separate, label='OpenCL, vārdi glabāti atsevišķi')

    #plt.plot(x, y_cl_concat, label='OpenCL, words concatenated')
    plt.plot(x, y_cl_concat, label='OpenCL, saķēdēti vārdi')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'separate_vs_concat')

def cmp_compressed_uncompressed():
    filename='cmp_compressed_uncompressed.txt'
    [x, y_cuda_uncompressed, y_cuda_compressed, y_cl_uncompressed, y_cl_compressed]=np.loadtxt(filename)

    #plt.plot(x, y_cuda_uncompressed, label='CUDA, symbols uncompressed')
    plt.plot(x, y_cuda_uncompressed, label='CUDA, nesaspiesti simboli')

    #plt.plot(x, y_cuda_compressed, label='CUDA, symbols compressed')
    plt.plot(x, y_cuda_compressed, label='CUDA, saspiesti simboli')

    #plt.plot(x, y_cl_uncompressed, label='OpenCL, symbols uncompressed')
    plt.plot(x, y_cl_uncompressed, label='OpenCL, nesaspiesti simboli')

    #plt.plot(x, y_cl_compressed, label='OpenCL, symbols compressed')
    plt.plot(x, y_cl_compressed, label='OpenCL, saspiesti simboli')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'compressed_symbols')

def cmp_access():
    filename='cmp_access_8_32.txt'
    [x, y_cuda_8, y_cuda_32, y_cl_8, y_cl_32]=np.loadtxt(filename)

    #plt.plot(x, y_cuda_8, label='CUDA, read/write 8 bits')
    plt.plot(x, y_cuda_8, label='CUDA, vārdu lasīšana un rakstīšana pa 8 bitiem')

    #plt.plot(x, y_cuda_32, label='CUDA, read/write 32 bits')
    plt.plot(x, y_cuda_32, label='CUDA, vārdu lasīšana un rakstīšana pa 32 bitiem')

    #plt.plot(x, y_cl_8, label='OpenCL, read/write 8 bits')
    plt.plot(x, y_cl_8, label='OpenCL, vārdu lasīšana un rakstīšana pa 8 bitiem')

    #plt.plot(x, y_cl_32, label='OpenCL, read/write 32 bits')
    plt.plot(x, y_cl_32, label='OpenCL, vārdu lasīšana un rakstīšana pa 32 bitiem')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'uint8_vs_uint32')

def cmp_store_patterns():
    filename='cmp_store_patterns.txt'
    [x, y_cuda_concat, y_cuda_interleaved, y_cuda_interleaved_pattern,
     y_cl_concat, y_cl_interleaved, y_cl_interleaved_pattern]=np.loadtxt(filename)

    #plt.plot(x, y_cuda_concat, label='CUDA, concatenated words')
    #plt.plot(x, y_cuda_concat, label='CUDA, saķēdēti vārdi')

    #plt.plot(x, y_cuda_interleaved, label='CUDA, interleaved words')
    #plt.plot(x, y_cuda_interleaved, label='CUDA, starpliktie vārdi')

    #plt.plot(x,y_cuda_interleaved_pattern, label='CUDA, starpliktā pieejas metode')

    #plt.plot(x, y_cl_concat, label='OpenCL, concatenated words')
    plt.plot(x, y_cl_concat, label='OpenCL, saķēdēti vārdi')

    #plt.plot(x, y_cl_interleaved, label='OpenCL, interleaved words')
    plt.plot(x, y_cl_interleaved, label='OpenCL, starpliktie vārdi')

    plt.plot(x,y_cl_interleaved_pattern, label='OpenCL, starpliktā pieejas metode')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'concat_vs_interleaved')

def cmp_square_nonsquare():
    filename='cmp_square_nonsquare.txt'
    [x, y_cuda, y_cuda_square, y_cl, y_cl_square]=np.loadtxt(filename)

    plt.plot(x, y_cuda, label='CUDA, nepalielināts automāts')

    plt.plot(x, y_cuda_square, label='CUDA, kvadrātisks automāts')

    plt.plot(x, y_cl, label='OpenCL, nepalielināts automāts')

    plt.plot(x, y_cl_square, label='OpenCL, kvadrātisks automāts')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'square_vs_nonsquare')

def cmp_compact():
    filename='cmp_compact.txt'
    temp=read_vector_vector(filename)

    [x, y_cuda_uncompact, y_cuda_compact] = temp

    #plt.plot(x, y_cuda_uncompact, label='Not compact automaton representation, CUDA')
    plt.plot(x, y_cuda_uncompact, label='CUDA, nesaspiesta automāta tabulas glabāšana')

    [y_cuda_compact, x]=trim_both(y_cuda_compact, x)
    #plt.plot(x, y_cuda_compact, label='Compact automaton representation, CUDA')
    plt.plot(x, y_cuda_compact, label='CUDA, saspiesta automāta tabulas glabāšana')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'compact_auto')

def cmp_constant_global():
    filename='cmp_const_glob.txt'
    temp=read_vector_vector(filename)

    [x, y_cuda_global, y_cuda_constant] = temp

    #plt.plot(x, y_cuda_global, label='CUDA, automaton in global memory')
    plt.plot(x, y_cuda_global, label='CUDA, automāts globālajā atmiņā')

    [y_cuda_constant, x]=trim_both(y_cuda_constant, x)
    #plt.plot(x, y_cuda_constant, label='CUDA, automaton in constant memory')
    plt.plot(x, y_cuda_constant, label='CUDA, automāts konstantajā atmiņā')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'global_vs_const')

def cmp_texture_global():
    filename='cmp_glob_texture.txt'
    temp=read_vector_vector(filename)

    [x, y_cl_global, y_cl_text] = temp

    #plt.plot(x, y_cl_global, label='OpenCL, automaton in global memory')
    plt.plot(x, y_cl_global, label='OpenCL, automāts globālajā atmiņā')

    [y_cl_text, x]=trim_both(y_cl_text, x)
    #plt.plot(x, y_cl_text, label='OpenCL, automaton in texture memory')
    plt.plot(x, y_cl_text, label='OpenCL, automāts tekstūratmiņā')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'global_vs_texture')

def cmp_pinned_unpinned():
    filename='cmp_pinned.txt'
    temp=read_vector_vector(filename)

    [x, y_cl_unpinned, y_cl_pinned] = temp

    plt.plot(x, y_cl_unpinned, label='OpenCL, vārdi nebloķētajā saimniekatmiņā')
    plt.plot(x, y_cl_pinned, label='OpenCL, vārdi bloķētajā saimniekatmiņā')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'unpinned_vs_pinned')

def cmp_all_cpu_gpu():
    filename='cmp_all_cpu_gpu.txt'
    temp=read_vector_vector(filename)

    [x, y_cpu_native, y_cuda, y_cl_gpu, y_cl_cpu,
     y_cuda_dummy, y_cl_gpu_dummy, y_cl_cpu_dummy] = temp

    plt.plot(x, y_cpu_native, label='Natīvs C++, viens pavediens')
    plt.plot(x, y_cuda, label='CUDA')
    plt.plot(x, y_cl_gpu, label='OpenCL, GPU')
    plt.plot(x, y_cl_cpu, label='OpenCL, CPU')
    plt.plot(x, y_cuda_dummy, label='CUDA, tikai tabulas lasīšana')
    plt.plot(x, y_cl_gpu_dummy, label='OpenCL, GPU, tikai tabulas lasīšana')
    plt.plot(x, y_cl_cpu_dummy, label='OpenCL, CPU, tikai tabulas lasīšana')

    [wc, wl]=default_wc_wl()

    plt_common_part(wc, wl, 'all_cpu_gpu')

import math

def cmp_workgroup_sizes():
    filename='cmp_workgroup_size.txt'
    temp=read_vector_vector(filename)
    [x, y_cuda, y_cl_gpu, y_cl_cpu_concat, #y_cl_cpu_interleaved
    ]=temp

    x=[math.log2(xx) for xx in x]

    plt.plot(x, y_cuda, label='CUDA')
    plt.plot(x, y_cl_gpu, label='OpenCL, GPU')
    plt.plot(x, y_cl_cpu_concat, label='OpenCL, CPU')
    #plt.plot(x, y_cl_cpu_interleaved, label='OpenCL, CPU interleaved')

    plt.xlabel("log2(Darba grupas izmērs)")
    plt.ylabel("Izpildes ātrums (simboli/ms)")
    plt.legend()

    [wc, wl]=[1024,4096]

    plt.title(
        '\n'.join(
            ['Automāta ātrums = f(darba grupas izmērs)',
             str(int(wc)) + ' vārdi, vārdu garums - ' + str(int(wl)) +' simboli',
             'ieejas, izejas simboli - 4 biti',
             'Automāta stāvokļu skaits - 2^20'
            ])
    )

    lim_y=plt.ylim()
    plt.ylim(0, increase_plot_height*lim_y[1])
    plt.savefig('cmp_group_size'+'.png')
    #plt.savefig(img_name+'.pdf')
    plt.show()
    #plt.clf()

def cmp_complects():
    filename='cmp_different_complects.txt'
    temp=read_vector_vector(filename)
    [x, [state_bits, word_length], y_cpu, y_cuda, y_cl_gpu, y_cl_cpu, #y_cl_cpu_interleaved
    ]=temp

    x=[math.log2(xx) for xx in x]

    plt.plot(x, y_cpu, label='CPU, viens pavediens')
    plt.plot(x, y_cuda, label='CUDA')
    plt.plot(x, y_cl_gpu, label='OpenCL, GPU')
    plt.plot(x, y_cl_cpu, label='OpenCL, CPU')
    #plt.plot(x, y_cl_cpu_interleaved, label='OpenCL, CPU interleaved')

    plt.xlabel("log2(Vārdu skaits)")
    plt.ylabel("Izpildes ātrums (simboli/ms)")
    plt.legend()

    #[wc, wl]=[1024,4096]

    plt.title(
        '\n'.join(
            ['Automāta ātrums = f(vārdu skaits)',
             'vārdu garums - ' + str(int(word_length)) + ' simboli',
             #str(int(wc)) + ' vārdi, vārdu garums - ' + str(int(wl)) +' simboli',
             'ieejas, izejas simboli - 4 biti',
             'Automāta stāvokļu skaits - 2^' + str(int(state_bits))
            ])
    )

    lim_y=plt.ylim()
    plt.ylim(0, increase_plot_height*lim_y[1])
    plt.savefig('cmp_group_size'+'.png')
    #plt.savefig(img_name+'.pdf')
    plt.show()
    #plt.clf()

def read_vector_vector(filename):
    f=open(filename, mode='r')
    res=[]
    for line in f.readlines():
        res.append([ float(x) for x in line.rstrip().split(' ')])
    return res

def cmp_cpu_gpu():
    filename='cmp_cpu_gpu.txt'
    temp=read_vector_vector(filename)

    [x, wc_wl,
        y_cl_cpu,
        y_cl_gpu] = temp

    plt.plot(x, y_cl_cpu, label='OpenCL, CPU')
    plt.plot(x, y_cl_gpu, label='OpenCL, GPU')

    plt.legend()

    [word_count, word_length] = wc_wl

    plt.xlabel("State bits of automaton")
    plt.ylabel("Execution speed (symbols/ms)")
    plt.title(
        '\n'.join( [
            'Speed of transducer=f(automaton size)',
            'input words - ' + str(word_count),
            'input length -' + str(word_length) + ' symbols',
            'in, out symbols - 4 bits'
        ])
    )

    lim_y=plt.ylim()
    plt.ylim(0, increase_plot_height*lim_y[1])
    plt.show()

#all_txt()
#all_new_txt()
# cmp_refined_unrefined_int()
# input("Press Enter to view next plot...")
# cmp_compact()
# input("Press Enter to view next plot...")
# cmp_cpu_gpu()
#input("Press Enter to continue...")

#cmp_refined_unrefined_int(16392, 512)
#cmp_refined_unrefined_int(16392, 8192)
#cmp_refined_unrefined_int_big()
#cmp_refined_unrefined_int_small()
#variance_test()

#cmp_contiguous_uncontiguous()
#cmp_compressed_uncompressed()
#cmp_access()
#cmp_store_patterns()
cmp_square_nonsquare()
#cmp_compact()
#cmp_constant_global()
#cmp_texture_global()
#cmp_pinned_unpinned()
#cmp_all_cpu_gpu()
#cmp_workgroup_sizes()

#cmp_complects()
