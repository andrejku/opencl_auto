/*
Minimalistic example project which uses OpenCL_Auto.
Steps made to get this working (platform: x64, configuration: Debug)

1. Solution -> Add -> New project -> Visual C++/Empty project -> *change name to Example_CL* -> Ok

2. Example_CL -> Properties -> Common Properties -> Frameworks and references ->
	-> Add new reference -> Check OpenCL_Auto

3. [Example_CL -> Properties -> Congiguration Properties := Conf] ->
	-> Add platform and Configuration, which you need (Debug and x64 in my case)

4. Conf -> C/C++ -> General -> Additional Include Directories -> change to:
$(CUDA_PATH)\include;$(SolutionDir)/Common;$(SolutionDir)/OpenCL_Auto;%(AdditionalIncludeDirectories);
	(to #include like "header.h", not "../Common/header.h")
	first - path to OpenCL header files (worked for me with Nvidia CUDA 5.5, win
	second - path to Common project
	third - path to OpenCL_Auto project

5. Conf -> Linker -> General -> Additional Library Directories -> change to:
$(OutDir);%(AdditionalLibraryDirectories);
	(OpenCL_Auto.lib will be in solution output directory)

6. Conf -> Linker -> Input -> Additional Dependencies -> OpenCL_Auto.lib;%(AdditionalDependencies)
--------------

Note: if building for platform other than x64 and configuration other than Debug/Release,
changes in OpenCL_Auto and/or Common projects would need to be made
(because settings for only these 2 configs are there; and because OpenCL_Auto references CUDAs x64 OpenCL.lib)
*/

#include <iostream>
#include "automaton_host.h"
#include "CL_helper.h"
#include "automaton_CL.h"
#include "cl_run_automaton.h"

using namespace std;

int main()
{
	common_auto::automaton_host A(1,4,4);
	// from every state go to 0, 0 as output
	for(int i=0; i<A.table_size; i++) A.table[i]=0U;

	cl::Context context=cl_auto::get_first_GPU_context();
	cl_auto::automaton_CL cl_A(A, context);

	const std::string filename="interleaved_uint32.cl";
	const std::string kernel_name="run_automaton_symbols_interleaved_uint32";

	// have to know both location of kernel file and name of kernel function
	cl::Kernel kernel=cl_auto::create_kernel(context, filename, kernel_name, "");

	const int WORD_COUNT=2;
	const int WORD_LENGTH=2; // in uint32_t units
	
	uint32_t input[WORD_COUNT*WORD_LENGTH];
	uint32_t output[WORD_COUNT*WORD_LENGTH];

	for(int i=0; i<WORD_COUNT*WORD_LENGTH; i++) input[i]=0U; // every input symbol is 0
	cl_auto::cl_run_automaton_symbols_interleaved_uint32(
		cl_A,
		kernel,
		WORD_COUNT,
		WORD_LENGTH,
		input,
		output,
		1
	);

	return 0;
}